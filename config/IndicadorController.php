<?php

namespace App\Http\Controllers;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class IndicadorController extends Controller {

    public function indicadoresOperador(Request $request) {
        
        try {
            $indicadores = [];
            $fHoy = Carbon::now('America/Bogota')->format('Y-m-d');
            $operador = User::find($request->input('users_id'));
            
            $indicadores["meta_hoy"] = $operador->meta_hoy;
            $aux = $operador->metaOperador()->where('fecha', $fHoy)->first();
            is_null($aux) ? $resultado = 0 : $resultado = $aux->realizado;
            $indicadores["realizado"] = $resultado;
            $indicadores["meta_global"] = $operador->meta_global;
            $indicadores["historico"] = $operador->activos()->count();

            return json_encode(["status" => "ok", "msj" => $indicadores]);
        } catch (Exception $exc) {
            return response()->json('error', 404);
        }
    }

    public function indicadoresSupervisor(Request $request) {

        $indicadores = [];
        $fHoy = Carbon::now('America/Bogota')->format('Y-m-d');
        $supervisor = User::findOrFail($request->input('users_id'));

        if ($supervisor) {
            $indicadores["meta_hoy"] = $supervisor->operarios()->sum('meta_hoy');
            $aux = $supervisor->metaOperador()->where('fecha', $fHoy)->first();
            is_null($aux) ? $resultado = 0 : $resultado = $aux->realizado;
            $indicadores["realizado"] = $resultado;
            $indicadores["meta_global"] = $supervisor->meta_global;
            $indicadores["historico"] = $supervisor->operarios()->activos()->count();
            return json_encode(["status" => "ok", "msj" => $indicadores]);
        }
        return response()->json('error_user', 404);
    }

}
