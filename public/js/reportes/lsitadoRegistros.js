function verListadoRegistros(url) {
    var i = 1
    var token = $("input[name*='_token']").val();
    //var fechasRegistros = $("#fechasRegistros").val()
    var fdesde = $("#fdesde").val()
    var fhasta = $("#fhasta").val()
    var operadorRegistros = $("#operadorRegistros").val()

    $("#resultEi").html("<center><img src='" + url + "/img/cargando.gif' style='width: 5%;'><p>Por favor espere mientras se valida la información.</p></center>");
    $("#resutlTrafo").html("<center><img src='" + url + "/img/cargando.gif' style='width: 5%;'><p>Por favor espere mientras se valida la información.</p></center>");

    $.ajax({
        method: "POST",
        url: url + "/jsonRegistros",
        data: {
            _token: token,
            fdesde: fdesde,
            fhasta: fhasta,
            idop: operadorRegistros
        }
    }).done(function (result) {
        //console.log("RESULT - " + JSON.stringify(result))
        //var ret = JSON.parse(result)
        var tableRegistros = '<table border="1px" id="malditaTabla">'
        tableRegistros += '<thead>'
        tableRegistros += retHeader()
        tableRegistros += '</thead>'

        tableRegistros += '<tbody style="overflow: auto; height: 500px;" >'

        tableRegistros += getLumRef(result.luminarias, i, url)
        tableRegistros += getFarTerr(result.faroles, i, url)

        tableRegistros += '</tbody>'
        tableRegistros += '</table>'

        //var headEi = '<table class="table table-bordered" id="resultRegTable"><thead>'
        $("#resultEi").html(tableRegistros)

        var tableTrafos = '<table class="table table-bordered" id="resultRegTable">'
        tableTrafos += '<thead>'
        tableTrafos += retHeaderTrafo()
        tableTrafos += '</thead>'
        tableTrafos += '<tbody>'

        tableTrafos += getTrafos(result.trafos, i, url)

        tableTrafos += '</tbody>'
        tableTrafos += '</table>'
        $("#resutlTrafo").html(tableTrafos)

    });

}


function getTrafos(trafos, i, url) {
    var tableTrafos = ""

    $.each(trafos, function (ix, item) {
        tableTrafos += '<tr>'

        tableTrafos += '<td>' + item.id + '</td>'
        tableTrafos += '<td>' + item.apid + '</td>'
        tableTrafos += '<td>' + item.latitud.replace('.',',') + '</td>'
        tableTrafos += '<td>' + item.longitud.replace('.',',') + '</td>'
        tableTrafos += '<td>' + item.nombres + ' ' + item.apellidos + '</td>'
        tableTrafos += '<td>' + item.created_at + '</td>'
        tableTrafos += '<td></td>'
        tableTrafos += '<td>' + item.barrio + '</td>'
        if (item.serial) {
            tableTrafos += '<td>' + item.serial + '</td>'
        } else {
            tableTrafos += '<td></td>'
        }
        //Obs trafo
        tableTrafos += '<td>'+item.observacion+'</td>'
        //APOYO
        if (item.apPertenece === '1') {
            if (item.apTipo === '1') {
                tableTrafos += '<td>' + item.apLong + '</td>'
            } else {
                tableTrafos += '<td></td>'
            }
            if (item.apTipo === '2') {
                tableTrafos += '<td>' + item.apLong + '</td>'
            } else {
                tableTrafos += '<td></td>'
            }
            if (item.apTipo === '3') {
                tableTrafos += '<td>' + item.apLong + '</td>'
            } else {
                tableTrafos += '<td></td>'
            }
            if (item.apTipo === '4') {
                tableTrafos += '<td>' + item.apLong + '</td>'
            } else {
                tableTrafos += '<td></td>'
            }
            if (item.apTipo === '5') {
                tableTrafos += '<td>' + item.apLong + '</td>'
            } else {
                tableTrafos += '<td></td>'
            }
            tableTrafos += '<td></td>'
            tableTrafos += '<td></td>'
            tableTrafos += '<td></td>'
            tableTrafos += '<td></td>'
        }
        if (item.apPertenece === '2') {
            tableTrafos += '<td></td>'
            tableTrafos += '<td></td>'
            tableTrafos += '<td></td>'
            tableTrafos += '<td></td>'
            tableTrafos += '<td></td>'
            tableTrafos += '<td>' + item.apLong + '</td>'
            tableTrafos += '<td></td>'
            tableTrafos += '<td></td>'
            tableTrafos += '<td></td>'
        }
        if (item.apPertenece === '3') {
            tableTrafos += '<td></td>'
            tableTrafos += '<td></td>'
            tableTrafos += '<td></td>'
            tableTrafos += '<td></td>'
            tableTrafos += '<td></td>'
            tableTrafos += '<td></td>'
            if (item.apTipo === '1') {
                tableTrafos += '<td>' + item.apLong + '</td>'
            } else {
                tableTrafos += '<td></td>'
            }
            if (item.apTipo === '2') {
                tableTrafos += '<td>' + item.apLong + '</td>'
            } else {
                tableTrafos += '<td></td>'
            }
            if (item.apTipo === '3') {
                tableTrafos += '<td>' + item.apLong + '</td>'
            } else {
                tableTrafos += '<td></td>'
            }
        }
        if (item.apPertenece === '0') {
            tableTrafos += '<td></td>'
            tableTrafos += '<td></td>'
            tableTrafos += '<td></td>'
            tableTrafos += '<td></td>'
            tableTrafos += '<td></td>'
            tableTrafos += '<td></td>'
            tableTrafos += '<td></td>'
            tableTrafos += '<td></td>'
            tableTrafos += '<td></td>'
        }

        //Obs apoyo
        tableTrafos += '<td>'+item.apObservacion+'</td>'


        tableTrafos += '<td>' + item.potencia + '</td>'

        //NOVEDADES Apoyo
        if (item.apNovedades) {
            if (item.apNovedades.indexOf("Valla publicitaria") > -1) {
                tableTrafos += '<td>x</td>'
            } else {
                tableTrafos += '<td></td>'
            }
            if (item.apNovedades.indexOf("Poste desplomado") > -1) {
                tableTrafos += '<td>x</td>'
            } else {
                tableTrafos += '<td></td>'
            }
            if (item.apNovedades.indexOf("Poste quebrado") > -1) {
                tableTrafos += '<td>x</td>'
            } else {
                tableTrafos += '<td></td>'
            }
            if (item.apNovedades.indexOf("Poste caído") > -1) {
                tableTrafos += '<td>x</td>'
            } else {
                tableTrafos += '<td></td>'
            }
            if (item.apNovedades.indexOf("Poste corroído") > -1) {
                tableTrafos += '<td>x</td>'
            } else {
                tableTrafos += '<td></td>'
            }
            if (item.apNovedades.indexOf("No aplica") > -1) {
                tableTrafos += '<td>x</td>'
            } else if (item.apNovedades.indexOf("NO APLICA - REFLECTOR") > -1) {
                tableTrafos += '<td>x</td>'
            } else {
                tableTrafos += '<td></td>'
            }


        } else {
            tableTrafos += '<td></td>'
            tableTrafos += '<td></td>'
            tableTrafos += '<td></td>'
            tableTrafos += '<td></td>'
            tableTrafos += '<td></td>'
            tableTrafos += '<td>x</td>'
        }

        tableTrafos += '<td><a href="' + url + '/storage/fotos/' + item.foto + '" target="_blank">Ver foto</a></td>'
        i++
    })

    return tableTrafos
}

function getFarTerr(faroles, i, url) {
    var tableRegistros = ""
    $.each(faroles, function (ix, item) {
        tableRegistros += '<tr>'

        tableRegistros += '<td>' + item.eiid + '</td>'
        tableRegistros += '<td></td>'
        if (item.nomenclatura) {
            tableRegistros += '<td>' + item.nomenclatura + '</td>'
        } else {
            tableRegistros += '<td></td>'
        }
        if (item.direccion) {
            tableRegistros += '<td>' + item.direccion + '</td>'
        } else {
            tableRegistros += '<td></td>'
        }
        tableRegistros += '<td>' + item.barrio + '</td>'
        tableRegistros += '<td>' + item.latitud.replace('.',',') + '</td>'
        tableRegistros += '<td>' + item.longitud.replace('.',',') + '</td>'
        tableRegistros += '<td>' + item.nombres + ' ' + item.apellidos + '</td>'
        tableRegistros += '<td>' + item.fecha_registro + '</td>'
        tableRegistros += '<td>' + item.hora + '</td>'
        tableRegistros += '<td></td>'
        if (item.serial) {
            tableRegistros += '<td>' + item.serial + '</td>'
        } else {
            tableRegistros += '<td></td>'
        }

        //ELEMENTO DE ILUMINACION FAROL TERRA
        if (item.tipo === 2) {
            if (item.tecnologia) {
                if (item.tecnologia === "sodio") {
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td>' + item.potencia.replace('W','') + '</td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                }
                if (item.tecnologia === "led") {
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td>' + item.potencia.replace('W','') + '</td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                }
            } else {
                tableRegistros += '<td></td>'
                tableRegistros += '<td></td>'
                tableRegistros += '<td></td>'
                tableRegistros += '<td></td>'
                tableRegistros += '<td></td>'
                tableRegistros += '<td></td>'
                tableRegistros += '<td></td>'
                tableRegistros += '<td></td>'
                tableRegistros += '<td></td>'
                tableRegistros += '<td></td>'
                tableRegistros += '<td></td>'
                tableRegistros += '<td></td>'
                tableRegistros += '<td></td>'
                tableRegistros += '<td></td>'
            }
        }

        if (item.tipo === 4) {

            if (item.tecnologia) {
                if (item.tecnologia === "sodio") {
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td>' + item.potencia.replace('W','') + '</td>'
                    tableRegistros += '<td></td>'
                }
                if (item.tecnologia === "led") {
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td>' + item.potencia.replace('W','') + '</td>'
                }
            } else {
                tableRegistros += '<td></td>'
                tableRegistros += '<td></td>'
                tableRegistros += '<td></td>'
                tableRegistros += '<td></td>'
                tableRegistros += '<td></td>'
                tableRegistros += '<td></td>'
                tableRegistros += '<td></td>'
                tableRegistros += '<td></td>'
                tableRegistros += '<td></td>'
                tableRegistros += '<td></td>'
                tableRegistros += '<td></td>'
                tableRegistros += '<td></td>'
                tableRegistros += '<td></td>'
                tableRegistros += '<td></td>'
            }
        }

        //Obs ei
        if(item.ilumObservacion){
            tableRegistros += '<td>' + item.ilumObservacion + '</td>'
        }else{
            tableRegistros += '<td></td>'
        }

        //apoyo y brazo
        tableRegistros += '<td></td>'
        tableRegistros += '<td></td>'
        tableRegistros += '<td></td>'
        tableRegistros += '<td></td>'
        tableRegistros += '<td></td>'
        tableRegistros += '<td></td>'
        tableRegistros += '<td></td>'
        tableRegistros += '<td></td>'
        tableRegistros += '<td></td>'
        tableRegistros += '<td></td>'
        tableRegistros += '<td></td>'
        tableRegistros += '<td></td>'
        tableRegistros += '<td></td>'

        //RED
        if (item.redTipo === 1) {
            tableRegistros += '<td>x</td>'
            tableRegistros += '<td></td>'
            tableRegistros += '<td></td>'
        }
        if (item.redTipo === 2) {
            tableRegistros += '<td></td>'
            if (item.redAerea === 1) {
                tableRegistros += '<td>Aerea</td>'
            } else if (item.redAerea === 0) {
                tableRegistros += '<td>Subterranea</td>'
            } else {
                tableRegistros += '<td></td>'
            }
            tableRegistros += '<td>' + item.redDistancia + '</td>'
        }

        //OBS red
        if(item.redObservacion){
            tableRegistros += '<td>' + item.redObservacion + '</td>'
        }else{
            tableRegistros += '<td></td>'
        }

        //NOVEDADES LUMINARIA
        if (item.lumNovedades) {
            if (item.lumNovedades.indexOf("Elemento Público") > -1) {
                tableRegistros += '<td>Elemento Público</td>'
            } else if (item.lumNovedades.indexOf("Elemento Privado") > -1) {
                tableRegistros += '<td>Elemento Privado</td>'
            } else {
                tableRegistros += '<td>Elemento Público</td>'
            }
            if (item.lumNovedades.indexOf("Iluminación caída") > -1) {
                tableRegistros += '<td>x</td>'
            } else {
                tableRegistros += '<td></td>'
            }
            if (item.lumNovedades.indexOf("Sin bombillo") > -1) {
                tableRegistros += '<td>x</td>'
            } else {
                tableRegistros += '<td></td>'
            }
            if (item.lumNovedades.indexOf("Directa") > -1) {
                tableRegistros += '<td>x</td>'
            } else {
                tableRegistros += '<td></td>'
            }
            if (item.lumNovedades.indexOf("Elemento roto") > -1) {
                tableRegistros += '<td>x</td>'
            } else {
                tableRegistros += '<td></td>'
            }
            if (item.lumNovedades.indexOf("Bombillo expuesto") > -1) {
                tableRegistros += '<td>x</td>'
            } else {
                tableRegistros += '<td></td>'
            }
            if (item.lumNovedades.indexOf("Necesario Poda") > -1) {
                tableRegistros += '<td>x</td>'
            } else {
                tableRegistros += '<td></td>'
            }
            if (item.lumNovedades.indexOf("No aplica") > -1) {
                tableRegistros += '<td>x</td>'
            } else {
                tableRegistros += '<td></td>'
            }
        } else {
            tableRegistros += '<td></td>'
            tableRegistros += '<td></td>'
            tableRegistros += '<td></td>'
            tableRegistros += '<td></td>'
            tableRegistros += '<td></td>'
            tableRegistros += '<td></td>'
            tableRegistros += '<td></td>'
            tableRegistros += '<td>x</td>'
        }


        //NOVEDADES Apoyo
        tableRegistros += '<td></td>'
        tableRegistros += '<td></td>'
        tableRegistros += '<td></td>'
        tableRegistros += '<td></td>'
        tableRegistros += '<td></td>'
        tableRegistros += '<td></td>'


        //Novedades del brazo
        tableRegistros += '<td></td>'
        tableRegistros += '<td></td>'
        tableRegistros += '<td></td>'


        //Novedades RED
        if (item.redNovedades) {
            if (item.redNovedades === "Red Abierta") {
                tableRegistros += '<td>x</td>'
            } else {
                tableRegistros += '<td></td>'
            }
            if (item.redNovedades === "Red Encauchetada") {
                tableRegistros += '<td>x</td>'
            } else {
                tableRegistros += '<td></td>'
            }
            if (item.redNovedades === "No aplica") {
                tableRegistros += '<td>x</td>'
            } else {
                tableRegistros += '<td></td>'
            }
        } else {
            tableRegistros += '<td></td>'
            tableRegistros += '<td></td>'
            tableRegistros += '<td>x</td>'
        }

        //FOTO
        tableRegistros += '<td><a href="' + url + '/storage/fotos/' + item.foto + '" target="_blank">Ver foto</a></td>'

        tableRegistros += '</tr>'
        i++
    })
    return tableRegistros
}

function getLumRef(luminarias, i, url) {
    var tableRegistros = ""
    $.each(luminarias, function (ix, item) {
        tableRegistros += '<tr>'
        tableRegistros += '<td>' + item.eiid + '</td>'
        tableRegistros += '<td>' + item.apid + '</td>'
        if (item.nomenclatura) {
            tableRegistros += '<td>' + item.nomenclatura + '</td>'
        } else {
            tableRegistros += '<td></td>'
        }
        if (item.direccion) {
            tableRegistros += '<td>' + item.direccion + '</td>'
        } else {
            tableRegistros += '<td></td>'
        }
        tableRegistros += '<td>' + item.barrio + '</td>'
        tableRegistros += '<td>' + item.latitud.replace('.',',') + '</td>'
        tableRegistros += '<td>' + item.longitud.replace('.',',') + '</td>'
        tableRegistros += '<td>' + item.nombres + ' ' + item.apellidos + '</td>'
        tableRegistros += '<td>' + item.fecha_registro + '</td>'
        tableRegistros += '<td>' + item.hora + '</td>'
        tableRegistros += '<td></td>'
        if (item.serial) {
            tableRegistros += '<td>' + item.serial + '</td>'
        } else {
            tableRegistros += '<td></td>'
        }

        //ELEMENTO DE ILUMINACION LUMINARIA REFLECTOR
        //console.log("TIPO - " + item.tipo)
        if (item.tipo === 1) {
            //console.log("tecnologia - " + item.tecnologia)
            if (item.tecnologia) {
                if (item.tecnologia === "sodio") {
                    tableRegistros += '<td>' + item.potencia.replace('W','') + '</td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                }
                if (item.tecnologia === "led") {
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td>' + item.nombre + '</td>'
                    tableRegistros += '<td>' + item.potencia.replace('W','') + '</td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                }
                if (item.tecnologia === "metal halide") {
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td>' + item.potencia.replace('W','') + '</td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                }
                if (item.tecnologia === "mercurio") {
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td>' + item.potencia.replace('W','') + '</td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                }
                if (item.tecnologia === "luz mixta") {
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td>' + item.potencia.replace('W','') + '</td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                }
            } else {
                tableRegistros += '<td></td>'
                tableRegistros += '<td></td>'
                tableRegistros += '<td></td>'
                tableRegistros += '<td></td>'
                tableRegistros += '<td></td>'
                tableRegistros += '<td></td>'
                tableRegistros += '<td></td>'
                tableRegistros += '<td></td>'
                tableRegistros += '<td></td>'
                tableRegistros += '<td></td>'
                tableRegistros += '<td></td>'
                tableRegistros += '<td></td>'
                tableRegistros += '<td></td>'
                tableRegistros += '<td></td>'
                tableRegistros += '<td></td>'
            }

        }
        if (item.tipo === 3) {
            //console.log("tecnologia3 - " + item.tecnologia)
            if (item.tecnologia) {
                if (item.tecnologia === "sodio") {
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td>' + item.potencia.replace('W','') + '</td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                }
                if (item.tecnologia === "led") {
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td>' + item.potencia.replace('W','') + '</td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                }
                if (item.tecnologia === "metal halide") {
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td>' + item.potencia.replace('W','') + '</td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                }
                if (item.tecnologia === "mercurio") {
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td>' + item.potencia.replace('W','') + '</td>'
                    tableRegistros += '<td></td>'
                    tableRegistros += '<td></td>'
                }
            } else {
                tableRegistros += '<td></td>'
                tableRegistros += '<td></td>'
                tableRegistros += '<td></td>'
                tableRegistros += '<td></td>'
                tableRegistros += '<td></td>'
                tableRegistros += '<td></td>'
                tableRegistros += '<td></td>'
                tableRegistros += '<td></td>'
                tableRegistros += '<td></td>'
                tableRegistros += '<td></td>'
                tableRegistros += '<td></td>'
                tableRegistros += '<td></td>'
                tableRegistros += '<td></td>'
                tableRegistros += '<td></td>'
            }
        }

        //Obs ei
        if (item.ilmObservacion) {
            tableRegistros += '<td>' + item.ilmObservacion + '</td>'
        } else {
            tableRegistros += '<td></td>'
        }

        //APOYO
        if (item.apPertence === 1) {
            if (item.apTipo === 1) {
                tableRegistros += '<td>'+item.apLong+'</td>'
            } else {
                tableRegistros += '<td></td>'
            }
            if (item.apTipo === 2) {
                tableRegistros += '<td>'+item.apLong+'</td>'
            } else {
                tableRegistros += '<td></td>'
            }
            if (item.apTipo === 3) {
                tableRegistros += '<td>'+item.apLong+'</td>'
            } else {
                tableRegistros += '<td></td>'
            }
            if (item.apTipo === 4) {
                tableRegistros += '<td>'+item.apLong+'</td>'
            } else {
                tableRegistros += '<td></td>'
            }
            if (item.apTipo === 5) {
                tableRegistros += '<td>'+item.apLong+'</td>'
            } else {
                tableRegistros += '<td></td>'
            }
            tableRegistros += '<td></td>'
            tableRegistros += '<td></td>'
            tableRegistros += '<td></td>'
            tableRegistros += '<td></td>'
        }
        if (item.apPertence === 2) {
            tableRegistros += '<td></td>'
            tableRegistros += '<td></td>'
            tableRegistros += '<td></td>'
            tableRegistros += '<td></td>'
            tableRegistros += '<td></td>'
            tableRegistros += '<td>'+item.apLong+'</td>'
            tableRegistros += '<td></td>'
            tableRegistros += '<td></td>'
            tableRegistros += '<td></td>'
        }
        if (item.apPertence === 3) {
            tableRegistros += '<td></td>'
            tableRegistros += '<td></td>'
            tableRegistros += '<td></td>'
            tableRegistros += '<td></td>'
            tableRegistros += '<td></td>'
            tableRegistros += '<td></td>'
            if (item.apTipo === 1) {
                tableRegistros += '<td>'+item.apLong+'</td>'
            } else {
                tableRegistros += '<td></td>'
            }
            if (item.apTipo === 2) {
                tableRegistros += '<td>'+item.apLong+'</td>'
            } else {
                tableRegistros += '<td></td>'
            }
            if (item.apTipo === 3) {
                tableRegistros += '<td>'+item.apLong+'</td>'
            } else {
                tableRegistros += '<td></td>'
            }
        }
        if (item.apPertence === 0) {
            tableRegistros += '<td></td>'
            tableRegistros += '<td></td>'
            tableRegistros += '<td></td>'
            tableRegistros += '<td></td>'
            tableRegistros += '<td></td>'
            tableRegistros += '<td></td>'
            tableRegistros += '<td></td>'
            tableRegistros += '<td></td>'
            tableRegistros += '<td></td>'
        }

        //Obs apoyo
        if (item.apObservacion) {
            tableRegistros += '<td>' + item.apObservacion + '</td>'
        } else {
            tableRegistros += '<td></td>'
        }

        //BBRAZO
        //console.log("BRAZO: " + item.id + " -- " + item.brzTipo)
        if (item.brzTipo === 1) {
            if(item.brzIncorporado === 1){
                tableRegistros += '<td>Incorporado</td>'
                tableRegistros += '<td></td>'
            }else{
                tableRegistros += '<td>No Incorporado</td>'
                tableRegistros += '<td></td>'
            }
        }
        else if (item.brzTipo === 2) {
            if(item.brzIncorporado === 1){
                tableRegistros += '<td>Incorporado</td>'
                tableRegistros += '<td></td>'
            }else{
                tableRegistros += '<td>No Incorporado</td>'
                tableRegistros += '<td></td>'
            }
        }
        else {
            tableRegistros += '<td>Incorporado</td>'
            tableRegistros += '<td></td>'
        }

        //Obs brz
        if (item.brzObservacion) {
            tableRegistros += '<td>' + item.brzObservacion + '</td>'
        } else {
            tableRegistros += '<td></td>'
        }

        //RED
        if (item.redTipo === 1) {
            tableRegistros += '<td>x</td>'
            tableRegistros += '<td></td>'
            tableRegistros += '<td></td>'
        }
        if (item.redTipo === 2) {
            tableRegistros += '<td></td>'
            if (item.redAerea === 1) {
                tableRegistros += '<td>Aerea</td>'
            } else if (item.redAerea === 0) {
                tableRegistros += '<td>Subterranea</td>'
            } else {
                tableRegistros += '<td></td>'
            }
            tableRegistros += '<td>' + item.redDistancia + '</td>'
        }

        //Obs red
        if (item.redObservacion) {
            tableRegistros += '<td>' + item.redObservacion + '</td>'
        } else {
            tableRegistros += '<td></td>'
        }

        //NOVEDADES LUMINARIA
        if (item.lumNovedades) {
            console.log(item.lumNovedades)
            if (item.lumNovedades.indexOf("Elemento Público") > -1) {
                tableRegistros += '<td>Elemento Público</td>'
            } else if (item.lumNovedades.indexOf("Elemento Privado") > -1) {
                tableRegistros += '<td>Elemento Privado</td>'
            } else {
                tableRegistros += '<td>Elemento Público</td>'
            }
            if (item.lumNovedades.indexOf("Iluminación caída") > -1) {
                tableRegistros += '<td>x</td>'
            } else {
                tableRegistros += '<td></td>'
            }
            if (item.lumNovedades.indexOf("Sin bombillo") > -1) {
                tableRegistros += '<td>x</td>'
            } else {
                tableRegistros += '<td></td>'
            }
            if (item.lumNovedades.indexOf("Directa") > -1) {
                tableRegistros += '<td>x</td>'
            } else {
                tableRegistros += '<td></td>'
            }
            if (item.lumNovedades.indexOf("Elemento roto") > -1) {
                tableRegistros += '<td>x</td>'
            } else {
                tableRegistros += '<td></td>'
            }
            if (item.lumNovedades.indexOf("Bombillo expuesto") > -1) {
                tableRegistros += '<td>x</td>'
            } else {
                tableRegistros += '<td></td>'
            }
            if (item.lumNovedades.indexOf("Necesario Poda") > -1) {
                tableRegistros += '<td>x</td>'
            } else {
                tableRegistros += '<td></td>'
            }
            if (item.lumNovedades.indexOf("No aplica") > -1) {
                tableRegistros += '<td>x</td>'
            } else {
                tableRegistros += '<td></td>'
            }
        } else {
            tableRegistros += '<td></td>'
            tableRegistros += '<td></td>'
            tableRegistros += '<td></td>'
            tableRegistros += '<td></td>'
            tableRegistros += '<td></td>'
            tableRegistros += '<td></td>'
            tableRegistros += '<td></td>'
            tableRegistros += '<td>x</td>'
        }


        //NOVEDADES Apoyo
        if (item.apNovedades) {
            if (item.apNovedades.indexOf("Valla publicitaria") > -1) {
                tableRegistros += '<td>x</td>'
            } else {
                tableRegistros += '<td></td>'
            }
            if (item.apNovedades.indexOf("Poste desplomado") > -1) {
                tableRegistros += '<td>x</td>'
            } else {
                tableRegistros += '<td></td>'
            }
            if (item.apNovedades.indexOf("Poste quebrado") > -1) {
                tableRegistros += '<td>x</td>'
            } else {
                tableRegistros += '<td></td>'
            }
            if (item.apNovedades.indexOf("Poste caído") > -1) {
                tableRegistros += '<td>x</td>'
            } else {
                tableRegistros += '<td></td>'
            }
            if (item.apNovedades.indexOf("Poste corroído") > -1) {
                tableRegistros += '<td>x</td>'
            } else {
                tableRegistros += '<td></td>'
            }
            if (item.apNovedades.indexOf("No aplica") > -1) {
                tableRegistros += '<td>x</td>'
            } else if (item.apNovedades.indexOf("NO APLICA - REFLECTOR") > -1) {
                tableRegistros += '<td>x</td>'
            } else {
                tableRegistros += '<td></td>'
            }


        } else {
            tableRegistros += '<td></td>'
            tableRegistros += '<td></td>'
            tableRegistros += '<td></td>'
            tableRegistros += '<td></td>'
            tableRegistros += '<td></td>'
            tableRegistros += '<td>x</td>'
        }

        //Novedades del brazo
        if (item.brzNovedades) {
            if (item.brzNovedades.indexOf("Brazo dañado o torcido") > -1) {
                tableRegistros += '<td>x</td>'
            } else {
                tableRegistros += '<td></td>'
            }
            if (item.brzNovedades.indexOf("Brazo corroído") > -1) {
                tableRegistros += '<td>x</td>'
            } else {
                tableRegistros += '<td></td>'
            }
            if (item.brzNovedades.indexOf("No aplica") > -1) {
                tableRegistros += '<td>x</td>'
            } else if (item.brzNovedades.indexOf("NO APLICA - REFLECTOR") > -1) {
                tableRegistros += '<td>x</td>'
            } else {
                tableRegistros += '<td></td>'
            }
        } else {
            tableRegistros += '<td></td>'
            tableRegistros += '<td></td>'
            tableRegistros += '<td>x</td>'
        }

        //Novedades RED
        if (item.redNovedades) {
            if (item.redNovedades === "Red Abierta") {
                tableRegistros += '<td>x</td>'
            } else {
                tableRegistros += '<td></td>'
            }
            if (item.redNovedades === "Red Encauchetada") {
                tableRegistros += '<td>x</td>'
            } else {
                tableRegistros += '<td></td>'
            }
            if (item.redNovedades === "No aplica") {
                tableRegistros += '<td>x</td>'
            } else {
                tableRegistros += '<td></td>'
            }
        } else {
            tableRegistros += '<td></td>'
            tableRegistros += '<td></td>'
            tableRegistros += '<td>x</td>'
        }

        //FOTO
        tableRegistros += '<td><a href="' + url + '/storage/fotos/' + item.foto + '" target="_blank">Ver foto</a></td>'

        tableRegistros += '</tr>'
        i++
    });

    return tableRegistros
}

function retHeader() {
    var msj = ""

    msj += '<tr>'
    msj += '<th rowspan="4" id="tableHead">ID</th>'
    msj += '<th rowspan="4" id="tableHead">CONSECUTIVO POSTE</th>'
    msj += '<th rowspan="4" id="tableHead">NOMENCLATURA</th>'
    msj += '<th rowspan="4" id="tableHead">DIRECCION</th>'
    msj += '<th rowspan="4" id="tableHead">BARRIO</th>'
    msj += '<th rowspan="3" colspan="2" id="tableHead">COORDENADAS (Georreferenciación)</th>'
    msj += '<th rowspan="4" id="tableHead">OPERADOR</th>'
    msj += '<th rowspan="4" id="tableHead">FECHA REGISTRO</th>'
    msj += '<th rowspan="4" id="tableHead">HORA</th>'
    msj += '<th rowspan="4" id="tableHead">COMUNA</th>'
    msj += '<th rowspan="4" id="tableHead">Código</th>'
    msj += '<th colspan="14" id="tableHead">ELEMENTOS DE ILUMINACION</th>'
    msj += '<th rowspan="4" id="tableHead">Observacion</th>'
    msj += '<th colspan="9" id="tableHead">APOYO</th>'
    msj += '<th rowspan="4" id="tableHead">Observacion</th>'
    msj += '<th colspan="2" id="tableHead">BRAZO</th>'
    msj += '<th rowspan="4" id="tableHead">Observacion</th>'
    msj += '<th colspan="3" id="tableHead">REDES</th>'
    msj += '<th rowspan="4" id="tableHead">Observacion</th>'
    msj += '<th colspan="8" rowspan="3" id="tableHead">Novedades Luminaria</th>'
    msj += '<th colspan="6" rowspan="3" id="tableHead">Novedades Apoyo</th>'
    msj += '<th colspan="3" rowspan="3" id="tableHead">Novedades Brazo</th>'
    msj += '<th colspan="3" rowspan="3" id="tableHead">Novedades Red</th>'
    msj += '<th rowspan="4" id="tableHead">ENLACE FOTOS</th>'
    msj += '</tr>'
    msj += '<tr>'
    msj += '<th colspan="6" id="tableHead">Luminarias</th>'
    msj += '<th colspan="2" id="tableHead">Faroles</th>'
    msj += '<th colspan="4" id="tableHead">Reflectores</th>'
    msj += '<th colspan="2" id="tableHead">Terra</th>'
    msj += '<th colspan="5" id="tableHead">CENS</th>'
    msj += '<th id="tableHead">Telecom</th>'
    msj += '<th colspan="3" id="tableHead">Exclusivo AP</th>'
    msj += '<th rowspan="3" id="tableHead">Corto 1.20 mts</th>'
    msj += '<th rowspan="3" id="tableHead">Largo 2.40 mts</th>'
    msj += '<th rowspan="3" id="tableHead">Cens</th>'
    msj += '<th colspan="2" rowspan="2" id="tableHead">Exclusivos</th>'
    msj += '</tr>'
    msj += '<tr>'
    msj += '<th rowspan="2" id="tableHead">Sodio W</th>'
    msj += '<th colspan="2" id="tableHead">led W</th>'
    msj += '<th rowspan="2" id="tableHead">Metal Halide W</th>'
    msj += '<th rowspan="2" id="tableHead">Mercurio W</th>'
    msj += '<th rowspan="2" id="tableHead">Luz Mixta W</th>'
    msj += '<th rowspan="2" id="tableHead">Sodio W</th>'
    msj += '<th rowspan="2" id="tableHead">Led W</th>'
    msj += '<th rowspan="2" id="tableHead">Sodio W</th>'
    msj += '<th rowspan="2" id="tableHead">Led W</th>'
    msj += '<th rowspan="2" id="tableHead">Metal Halide W</th>'
    msj += '<th rowspan="2" id="tableHead">Mercurio W</th>'
    msj += '<th rowspan="2" id="tableHead">Sodio W</th>'
    msj += '<th rowspan="2" id="tableHead">Led W</th>'
    msj += '<th rowspan="2" id="tableHead">Concreto</th>'
    msj += '<th rowspan="2" id="tableHead">Metálico</th>'
    msj += '<th rowspan="2" id="tableHead">Fibra</th>'
    msj += '<th rowspan="2" id="tableHead">Riel</th>'
    msj += '<th rowspan="2" id="tableHead">Madera</th>'
    msj += '<th rowspan="2" id="tableHead">concreto</th>'
    msj += '<th rowspan="2" id="tableHead">concreto</th>'
    msj += '<th rowspan="2" id="tableHead">metalico</th>'
    msj += '<th rowspan="2" id="tableHead">fibra</th>'
    msj += '</tr>'
    msj += '<tr>'
    msj += '<th id="tableHead">Latitud</th>'
    msj += '<th id="tableHead">longitud</th>'
    msj += '<th id="tableHead">Tipo</th>'
    msj += '<th id="tableHead">Pot.</th>'
    msj += '<th id="tableHead">Aerea / Subterranea</th>'
    msj += '<th id="tableHead">Distancia</th>'
    msj += '<th id="tableHead">Elemento (Publico o Privado)</th>'
    msj += '<th id="tableHead">Iluminación caída</th>'
    msj += '<th id="tableHead">Sin bombillo</th>'
    msj += '<th id="tableHead">Directa</th>'
    msj += '<th id="tableHead">Elemento roto</th>'
    msj += '<th id="tableHead">Bombillo expuesto</th>'
    msj += '<th id="tableHead">Necesario Poda</th>'
    msj += '<th id="tableHead">No aplica</th>'
    msj += '<th id="tableHead">Valla publicitaria</th>'
    msj += '<th id="tableHead">Poste desplomado</th>'
    msj += '<th id="tableHead">Poste quebrado</th>'
    msj += '<th id="tableHead">Poste caído</th>'
    msj += '<th id="tableHead">Poste corroído</th>'
    msj += '<th id="tableHead">No aplica</th>'
    msj += '<th id="tableHead">Brazo dañado o torcido</th>'
    msj += '<th id="tableHead">Brazo corroido</th>'
    msj += '<th id="tableHead">No aplica</th>'
    msj += '<th id="tableHead">Red Abierta</th>'
    msj += '<th id="tableHead">Red Encauchetada</th>'
    msj += '<th id="tableHead">No aplica</th>'
    msj += '</tr>'

    return msj
}

function retHeaderTrafo() {
    var msj = ""

    msj += '<tr>'
    msj += '<th rowspan="3" id="tableHead">ID</th>'
    msj += '<th rowspan="3" id="tableHead">CONSECUTIVO POSTE</th>'
    msj += '<th rowspan="2" colspan="2" id="tableHead">COORDENADAS (Georreferenciación)</th>'
    msj += '<th rowspan="3" id="tableHead">OPERADOR</th>'
    msj += '<th rowspan="3" id="tableHead">FECHA REGISTRO</th>'
    msj += '<th rowspan="3" id="tableHead">COMUNA</th>'
    msj += '<th rowspan="3" id="tableHead">BARRIO</th>'
    msj += '<th rowspan="3" id="tableHead">Código</th>'
    msj += '<th rowspan="3" id="tableHead">Observacion</th>'
    msj += '<th colspan="9" id="tableHead">APOYO</th>'
    msj += '<th rowspan="3" id="tableHead">Observacion</th>'
    msj += '<th rowspan="3" id="tableHead">Transformadores</th>'
    msj += '<th colspan="6" id="tableHead">Novedades Apoyo</th>'
    msj += '<th rowspan="3" id="tableHead">ENLACE FOTOS</th>'
    msj += '</tr>'
    msj += '<tr>'
    msj += '<th colspan="5" id="tableHead">CENS</th>'
    msj += '<th id="tableHead">Telecom</th>'
    msj += '<th colspan="3" id="tableHead">Exclusivo AP</th>'
    msj += '<th rowspan="2" id="tableHead">Valla publicitaria</th>'
    msj += '<th rowspan="2" id="tableHead">Poste desplomado.</th>'
    msj += '<th rowspan="2" id="tableHead">Poste desplomado.</th>'
    msj += '<th rowspan="2" id="tableHead">Poste caído.</th>'
    msj += '<th rowspan="2" id="tableHead">Poste corroído.</th>'
    msj += '<th rowspan="2" id="tableHead">No aplica</th>'
    msj += '</tr>'
    msj += '<tr>'
    msj += '<th id="tableHead">Latitud</th>'
    msj += '<th id="tableHead">Longitud</th>'
    msj += '<th id="tableHead">Concreto</th>'
    msj += '<th id="tableHead">Metálico</th>'
    msj += '<th id="tableHead">Fibra</th>'
    msj += '<th id="tableHead">Riel</th>'
    msj += '<th id="tableHead">Madera</th>'
    msj += '<th id="tableHead">Concreto</th>'
    msj += '<th id="tableHead">Concreto</th>'
    msj += '<th id="tableHead">Metálico</th>'
    msj += '<th id="tableHead">Fibra</th>'
    msj += '</tr>'

    return msj
}


function imprimirExcel(id) {
    $("#contenido_excel").val($("<div>").append($("#" + id).eq(0).clone()).html());
    $("#FormularioExportacion").submit();
}
