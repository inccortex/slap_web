function addMetaDia(meta, id, user, tipo) {
    var f = new Date();
    alertify
        .defaultValue(meta)
        .prompt("Meta de hoy: " + f.getDate() + "/" + (f.getMonth() + 1) + "/" + f.getFullYear() + " <br/> " + tipo + ": " + user,
            function (val, ev) {
                if (val === "" || val === undefined || val === 0) {

                } else {
                    $.ajax({
                        url: $("#url").val() + "/users/metaHoy",
                        method: "post",
                        data: {
                            user_id: id,
                            meta: val,
                            _token: $("input[name='_token']").val()
                        }
                    }).success(function (data) {
                        var resp = JSON.parse(data);
                        if (resp.status === "ok") {
                            alertify.success("Se ha almacenado la meta del usuario " + tipo + " " + user);
                            window.location.href = $("#url").val() + "/users";
                        } else if (resp.status === "fail") {
                            alertify.error(resp.msj);
                        }
                    }).error(function (err) {
                        alertify.error("Ha ocurrido un error interno, por favor verifique e intentelo de nuevo.");
                    });
                }

            }, function (ev) {
                alertify.error("You've clicked Cancel");
            });
}


function editar(id){
    location.href = "users/editar" + id;
}

function habilitarUser(id, nombres) {
    alertify
        .defaultValue("")
        .prompt("¿Está segur@ que desea habilitar el usuario " + nombres + "?<br/><br/>Si es así, por favor escriba la aplabara CONFIRMAR",
            function (val, ev) {
                if (val) {
                    if (val === "CONFIRMAR") {
                        $.ajax({
                            url: $("#url").val() + "/users/hablitar",
                            method: "POST",
                            data: {
                                id: id,
                                _token: $("input[name='_token']").val()
                            }
                        }).success(function (data) {
                            var resp = JSON.parse(data)
                            if (resp.status === "ok") {
                                alertify.success(resp.msj)
                                window.location.href = $("#url").val() + "/users"
                            } else if (resp.status === "fail") {
                                alertify.error(resp.msj)
                            }
                        }).error(function (err) {
                            alertify.error("Ha ocurrido un error interno, por favor verifique los datos, la conexión a internet, e intentelo de nuevo.")
                        })
                    } else {
                        alertify.error("Debe digitar la palabra correctamente para realizar la acción.")
                    }
                } else {
                    alertify.error("Debe escribir la plabra CONFIRMAR en el campo de texto para permitir habilitar el usuario.")
                }
            }, function (ev) {
                alertify.error("Se ha cancelado la acción.")
            })
}


function desHabilitarUser(id, nombres) {
    alertify
        .defaultValue("")
        .prompt("¿Está segur@ que desea deshabilitar el usuario " + nombres + "?<br/><br/>Si es así, por favor escriba la aplabara CONFIRMAR",
            function (val, ev) {
                if (val) {
                    if (val === "CONFIRMAR") {
                        $.ajax({
                            url: $("#url").val() + "/users/deshablitar",
                            method: "POST",
                            data: {
                                id: id,
                                _token: $("input[name='_token']").val()
                            }
                        }).success(function (data) {
                            var resp = JSON.parse(data)
                            if (resp.status === "ok") {
                                alertify.success(resp.msj)
                                window.location.href = $("#url").val() + "/users"
                            } else if (resp.status === "fail") {
                                alertify.error(resp.msj)
                            }
                        }).error(function (err) {
                            alertify.error("Ha ocurrido un error interno, por favor verifique los datos, la conexión a internet, e intentelo de nuevo.")
                        })
                    } else {
                        alertify.error("Debe digitar la palabra correctamente para realizar la acción.")
                    }
                } else {
                    alertify.error("Debe escribir la plabra CONFIRMAR en el campo de texto para permitir habilitar el usuario.")
                }
            }, function (ev) {
                alertify.error("Se ha cancelado la acción.")
            })
}
