function assignOp(idTab, idOp, idSup, oper, superv) {
    $("#" + idTab).html("<img src='" + $("#url").val() + "/img/cargando.gif' style='width: 50%;'>")
    var boton = '<button class="btn btn-sm btn-success" onclick="assignOp(\'' + idTab + '\',\'' + idOp + '\',\'' + idSup + '\',\'' + oper + '\',\'' + superv + '\')">'
    boton += '<i class="fa fa-plus"></i>'
    boton += '</button>'
    if (idOp && idSup) {
        alertify.confirm("¿Está seguro que desea asignar el operador <b>" + oper + "</b> al supervisor <b>" + superv + "</b>?", function () {
            $.ajax({
                url: $("#url").val() + "/users/assignOpSup",
                method: "POST",
                data: {
                    idOp: idOp,
                    idSup: idSup,
                    _token: $("input[name='_token']").val()
                }
            }).success(function (data) {
                var resp = JSON.parse(data)
                if (resp.status === "ok") {
                    alertify.success(resp.msj)
                    window.location.href = $("#url").val() + "/users/assingOperatos/" + idSup
                } else if (resp.status === "fail") {
                    $("#" + idTab).html(boton)
                    alertify.error(resp.msj)
                }
            }).error(function (err) {
                $("#" + idTab).html(boton)
                alertify.error("Ha ocurrido un error interno, por favor verifique los datos y la conexión a internet e intentelo de nuevo.")
            })
        }, function () {
            $("#" + idTab).html(boton)
            alertify.error("Se ha cancelado la acción.")
        });
    } else {
        alertify.error("No se reconoce la información del usuario para asignar, por favor recargue la página e intentelo de nuevo.")
    }

}

function desAssignOp(idTab, idOp, idSup, oper, superv) {
    $("#" + idTab).html("<img src='" + $("#url").val() + "/img/cargando.gif' style='width: 50%;'>")
    var boton = '<button class="btn btn-sm btn-success" onclick="assignOp(\'' + idTab + '\',\'' + idOp + '\',\'' + idSup + '\',\'' + oper + '\',\'' + superv + '\')">'
    boton += '<i class="fa fa-plus"></i>'
    boton += '</button>'
    if (idOp && idSup) {
        alertify.confirm("¿Está seguro que desea asignar el operador <b>" + oper + "</b> al supervisor <b>" + superv + "</b>?", function () {
            $.ajax({
                url: $("#url").val() + "/users/desAssignOpSup",
                method: "POST",
                data: {
                    idOp: idOp,
                    idSup: idSup,
                    _token: $("input[name='_token']").val()
                }
            }).success(function (data) {
                var resp = JSON.parse(data)
                if (resp.status === "ok") {
                    alertify.success(resp.msj)
                    window.location.href = $("#url").val() + "/users/assingOperatos/" + idSup
                } else if (resp.status === "fail") {
                    $("#" + idTab).html(boton)
                    alertify.error(resp.msj)
                }
            }).error(function (err) {
                $("#" + idTab).html(boton)
                alertify.error("Ha ocurrido un error interno, por favor verifique los datos y la conexión a internet e intentelo de nuevo.")
            })
        }, function () {
            $("#" + idTab).html(boton)
            alertify.error("Se ha cancelado la acción.")
        });
    } else {
        alertify.error("No se reconoce la información del usuario para desasignar, por favor recargue la página e intentelo de nuevo.")
    }
}