function login() {
    var user = $("#user").val()
    var password = $("#password").val()
    var validate = 0
    var msj = "<ul>"

    //Validacion de usuario digitado
    if (user === "" || user === undefined) {
        $("#user").css("border-color", "red")
        validate++
        msj += "<li>El nombre de usuario es requerido</li>"
    } else {
        $("#user").css("border-color", "#d2d6de")
    }

    //Validación de contraseña digitada
    if (password === "" || password === undefined) {
        $("#password").css("border-color", "red")
        validate++
        msj += "<li>La contraseña es requerida</li>"
    } else {
        $("#password").css("border-color", "#d2d6de")
    }

    //validar que los datos esten llenos para el inicio de sesión
    if (validate === 0) {
        alertify.success("Se han verificado los datos de acceso, se esta iniciando sesión.")
        $("#formLoginWeb").submit()
    } else {
        alertify.error("Debe llenar los campos para iniciar sesión: <br/> " + msj)
        return
    }

}