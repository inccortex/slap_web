<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>SLAP - Administración</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" href="{{URL::to("admin_template/bootstrap/css/bootstrap.min.css")}}">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="{{URL::to("admin_template/dist/css/AdminLTE.min.css")}}">
        <!-- iCheck -->
        <link rel="stylesheet" href="{{URL::to("admin_template/plugins/iCheck/square/blue.css")}}">

        <link rel="stylesheet" href="{{URL::to("js/alertify/css/alertify.css")}}">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="hold-transition login-page">
        <div class="login-box">
            <div class="login-logo">
                <a href="{{URL::to("/")}}"><b>SLAP</b> </a><br/>
                - Administración WEB -
            </div>
            <!-- /.login-logo -->
            <div class="login-box-body">
                <p class="login-box-msg">Inicie sesión para ingresar a la administración</p>

                {{Form::open(array("url" => "login", "method" => "post", "id" => "formLoginWeb"))}}
                <div class="form-group has-feedback">
                    <input type="text" name="user" id="user" class="form-control" placeholder="Usuario">
                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <input type="password" name="password" id="password" class="form-control" placeholder="Contraseña">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <button type="button" class="btn btn-primary btn-block btn-flat" onclick="login()">
                            <i class="fa fa-send"></i> Iniciar Sesión
                        </button>
                    </div>
                </div>
                {{Form::close()}}

            </div>

        </div>
        <div class="col-md-12">
            @if (!empty(session('error')))
            <div class="col-md-8 col-md-offset-2 mensaje" onclick="ocultar()" style="padding-top: 5%;">
                <div class="info-box bg-red">
                    <span class="info-box-icon"><i class="fa fa-warning"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">¡Error!</span>
                        <span class="description">
                            {{session('error')}}
                        </span>
                    </div>
                </div>
            </div>
            @elseif(!empty(session('msj')))
            <div class="col-md-8 col-md-offset-2 mensaje" onclick="ocultar()" style="padding-top: 5%;">
                <div class="info-box bg-green-active">
                    <span class="info-box-icon"><i class="fa fa-warning"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">¡Bien hecho!</span>
                        <span class="description">
                            {{session('msj')}}
                        </span>
                    </div>
                </div>
            </div>
            @endif
        </div>
        <!-- jQuery 2.2.0 -->
        <script src="{{URL::to("admin_template/plugins/jQuery/jQuery-2.2.0.min.js")}}"></script>
        <!-- Bootstrap 3.3.6 -->
        <script src="{{URL::to("admin_template/bootstrap/js/bootstrap.min.js")}}"></script>
        <!-- iCheck -->
        <script src="{{URL::to("admin_template/plugins/iCheck/icheck.min.js")}}"></script>

        <script src="{{URL::to("js/login/loginWeb.js")}}"></script>

        <script src="{{URL::to("js/alertify/js/alertify.js")}}"></script>

        <script>
                function ocultar() {
                    $(".mensaje").hide()
                }
        </script>

        <script>
            $(function () {
                $('input').iCheck({
                    checkboxClass: 'icheckbox_square-blue',
                    radioClass: 'iradio_square-blue',
                    increaseArea: '20%' // optional
                });
            });
        </script>
    </body>
</html>