@extends("template.templateRevisor")

@section("title")
    <h1>Inicio</h1>
@endsection

@section("navigation")
    <li>Inicio</li>
@endsection

@section("content")


    <div class="row">
        <div class="col-md-12">
            <!-- AREA CHART -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Indicadores Generales</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" onclick="traerInfo()">
                            <i class="fa  fa-refresh"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="chart">
                        <canvas id="areaChart" style="height: 400px; width: 400px;"></canvas>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->

        </div>
    </div>


    <input type="hidden" id="url" value="{{URL::to('/')}}">
    <script src="{{URL::to("admin_template/plugins/jQuery/jQuery-2.2.0.min.js")}}"></script>
    <script src="{{URL::to('admin_template/plugins/chartjs/Chart.js')}}"></script>
    <script>
        $(document).ready(function () {
            traerInfo()
        });

        function traerInfo() {
            console.log("Trayendo")
            $.ajax({
                method: "GET",
                url: $("#url").val() + "/datosGenerales"
            }).done(function (result) {
                console.log("RESULT - " + JSON.stringify(result))
                var datos = {
                    labels: [
                        'Luminarias: ' + result.luminarias,
                        'Faroles: ' + result.faroles,
                        'Reflectores: ' + result.reflectores,
                        'Terras: ' + result.terras,
                        'Transformadores: ' + result.trafos,
                        "Total Registrado: " + result.total,
                        "Meta General: 50000"
                    ],
                    datasets: [{
                        fillColor: "rgba(220,220,220,0.5)",
                        strokeColor: "rgba(220,220,220,0.8)",
                        highlightFill: "rgba(220,220,220,0.75)",
                        highlightStroke: "rgba(220,220,220,1)",
                        data: [
                            result.luminarias,
                            result.faroles,
                            result.reflectores,
                            result.terras,
                            result.trafos,
                            result.total,
                            50000
                        ]
                    }]
                };

                var canvas = document.getElementById("areaChart").getContext('2d')
                window.char = new Chart(canvas).Bar(datos)
            });
        }
    </script>
@endsection

