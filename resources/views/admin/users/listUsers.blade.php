@extends("template.template")

@section("title")
    <h1>Listados de Usuarios</h1>
@endsection

@section("navigation")
    <li>
        <a href="">
            <i class="fa fa-home"></i> Inicio
        </a>
    </li>
    <li>
        <i class="fa fa-users"></i> Usuarios
    </li>
@endsection

@section("content")

    <div class="nav-tabs-custom col-md-12">
        <ul class="nav nav-tabs">
            <li class="active">
                <a href="#op_habilitados" data-toggle="tab" aria-expanded="true">
                    <i class="fa fa-eye"></i> Operadores Habilitadors
                </a>
            </li>
            <li class="">
                <a href="#op_deshabilitados" data-toggle="tab" aria-expanded="false">
                    <i class="fa fa-eye-slash"></i> Operadores Deshabilitados
                </a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="op_habilitados">
                <div class="col-md-12">
                    <a href="{{URL::to('users/formNew')}}" class="btn btn-lg btn-info pull-right">
                        <i class="fa fa-plus"></i> Nuevo Usuario
                    </a>
                </div>
                <div class="col-md-12">
                    @if(count($usersOperadores) > 0)
                        <table class="table" id="operadores">
                            <thead>
                            <tr>
                                <th>Nombres</th>
                                <th>Apellidos</th>
                                <th>Email</th>
                                <th>Documento</th>
                                <th>Meta Hoy</th>
                                <th>Meta Global</th>
                                <th>Registros a Hoy</th>
                                <th>Meta Dia</th>
                                <th>Editar</th>
                                <th>Desactivar</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($usersOperadores as $usop)
                                <tr>
                                    <td>{{$usop->nombres}}</td>
                                    <td>{{$usop->apellidos}}</td>
                                    <td>{{$usop->email}}</td>
                                    <td>{{$usop->username}}</td>
                                    <td>{{intval($usop->meta_hoy)}}</td>
                                    <td>{{intval($usop->meta_global)}}</td>
                                    <td>{{intval($usop->activos()->count())}}</td>
                                    <td>
                                        <a href="#" class="btn btn-sm btn-info"
                                           onclick="addMetaDia('{{$usop->meta_hoy}}', '{{encrypt($usop->id)}}', '{{$usop->nombres}} {{$usop->apellidos}}', 'Operador')">
                                            <i class="fa fa-calendar"></i>
                                        </a>
                                    </td>
                                    <td>
                                        <a href="{{ url('users/editar', [encrypt($usop->id)]) }}"
                                           class="btn btn-sm btn-warning"><i class="fa fa-edit"></i></a>
                                    </td>
                                    <td>
                                        <a href="#" class="btn btn-sm btn-danger"
                                           onclick="desHabilitarUser('{{encrypt($usop->id)}}', '{{$usop->nombres}} {{$usop->apellidos}}')">
                                            <i class="fa fa-close"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        <span class="label label-warning">No se han registrado usuarios Operadores</span>
                    @endif
                </div>
            </div>
            <div class="tab-pane" id="op_deshabilitados">
                <div class="col-md-12">
                    @if(count($usersOperadoresD) > 0)
                        <table class="table" id="supervisores">
                            <thead>
                            <tr>
                                <th>Nombres</th>
                                <th>Apellidos</th>
                                <th>Email</th>
                                <th>Documento</th>
                                <th>Meta Global</th>
                                <th>Registro Global</th>
                                <th>Habilitar</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($usersOperadoresD as $usop)
                                <tr>
                                    <td>{{$usop->nombres}}</td>
                                    <td>{{$usop->apellidos}}</td>
                                    <td>{{$usop->email}}</td>
                                    <td>{{$usop->username}}</td>
                                    <td>{{intval($usop->meta_global)}}</td>
                                    <td>{{intval($usop->activos()->count())}}</td>
                                    <td>
                                        <button type="button" class="btn btn-sm btn-info"
                                                onclick="habilitarUser('{{encrypt($usop->id)}}', '{{$usop->nombres}} {{$usop->apellidos}}')">
                                            <i class="fa fa-check"></i>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        <span class="label label-warning">No se han deshabilitado usuarios Operadores</span>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="nav-tabs-custom col-md-12">
        <ul class="nav nav-tabs">
            <li class="active">
                <a href="#sup_habilitados" data-toggle="tab" aria-expanded="true">
                    <i class="fa fa-eye"></i> Supervisores Habilitadors
                </a>
            </li>
            <li class="">
                <a href="#sup_deshabilitados" data-toggle="tab" aria-expanded="false">
                    <i class="fa fa-eye-slash"></i> Supervisores Deshabilitados
                </a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="sup_habilitados">
                <div class="col-md-12">
                    <a href="" class="btn btn-lg btn-info pull-right">
                        <i class="fa fa-plus"></i> Nuevo Usuario
                    </a>
                </div>
                <div class="col-md-12">
                    @if(count($usersSupervisores) > 0)
                        <table class="table" id="supervisores">
                            <thead>
                            <tr>
                                <th>Nombres</th>
                                <th>Apellidos</th>
                                <th>Email</th>
                                <th>Documento</th>
                                <th>Meta Hoy</th>
                                <th>Meta Global</th>
                                <th>Registros a Hoy</th>
                                <th>Add operador</th>
                                <th>Editar</th>
                                <th>Desactivar</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($usersSupervisores as $usop)
                                <tr>
                                    <td>{{$usop->nombres}}</td>
                                    <td>{{$usop->apellidos}}</td>
                                    <td>{{$usop->email}}</td>
                                    <td>{{$usop->username}}</td>
                                    <td>{{intval($usop->meta_hoy)}}</td>
                                    <td>{{intval($usop->meta_global)}}</td>
                                    <td>{{intval($usop->meta_global)}}</td>
                                    <td>
                                        <a href="{{URL::to("users/assingOperatos/".encrypt($usop->id))}}"
                                           class="btn btn-sm btn-info">
                                            <i class="fa fa-users"></i>
                                        </a>
                                    </td>
                                    <td>
                                        <a href="{{URL::to('users/editar/'.encrypt($usop->id))}}" class="btn btn-sm btn-warning">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="btn btn-sm btn-danger"
                                            onclick="desHabilitarUser('{{encrypt($usop->id)}}', '{{$usop->nombres}} {{$usop->apellidos}}')">
                                            <i class="fa fa-close"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        <span class="label label-warning">No se han registrado usuarios Supervisores</span>
                    @endif
                </div>
            </div>
            <div class="tab-pane" id="sup_deshabilitados">
                <div class="col-md-12">
                    @if(count($usersSupervisoresD) > 0)
                        <table class="table" id="supervisores">
                            <thead>
                            <tr>
                                <th>Nombres</th>
                                <th>Apellidos</th>
                                <th>Email</th>
                                <th>Documento</th>
                                <th>Meta Hoy</th>
                                <th>Meta Global</th>
                                <th>Habilitar</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($usersSupervisoresD as $usop)
                                <tr>
                                    <td>{{$usop->nombres}}</td>
                                    <td>{{$usop->apellidos}}</td>
                                    <td>{{$usop->email}}</td>
                                    <td>{{$usop->username}}</td>
                                    <td>{{intval($usop->meta_global)}}</td>
                                    <td>{{intval($usop->meta_global)}}</td>
                                    <td>
                                        <button type="button" class="btn btn-sm btn-info"
                                                onclick="habilitarUser('{{encrypt($usop->id)}}', '{{$usop->nombres}} {{$usop->apellidos}}')">
                                            <i class="fa fa-check"></i>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        <span class="label label-warning">No se han deshabilitado usuarios Supervisores</span>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="url" value="{{URL::to('/')}}">
    {{ csrf_field() }}

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>

    <script>
        $(document).ready(function () {
            $('#operadores').DataTable();
            $('#supervisores').DataTable();
        });
    </script>
    <script src="{{URL::to('js/users/listUsers.js')}}"></script>

@endsection
