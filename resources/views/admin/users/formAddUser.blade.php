@extends("template.template")

@section("title")
<h1>Crear nuevo Usuario</h1>
@endsection

@section("navigation")
<li>
    <a href="{{URL::to('/')}}">
        <i class="fa fa-home"></i> Inicio
    </a>
</li>
<li>
    <i class="fa fa-plus"></i> Registrar Usuarios
</li>
@endsection

@section("content")
<input type='hidden' id='url' value='{{ URL::to('/') }}'>
<div class="col-md-12">
    <div class="row">
        <div class="box">
            <div class="box-body">
                <form role="form" method="POST" id="formAddUser" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="name">Nombres</label>
                            <input type="text" name="nombres" id="name" class="form-control"
                                   placeholder="Nombres completos del usuario" required="">
                        </div>
                        <div class="form-group">
                            <label for="apellidos">Apellidos</label>
                            <input type="text" name="apellidos" id="apellidos" class="form-control"
                                   placeholder="Apellidos completos del usuario" required="">
                        </div>
                        <div class="form-group">
                            <label for="username">Cédula</label>
                            <input type="number" name="user" id="username" min="1" class="form-control"
                                   placeholder="Número de cédula" required="">
                        </div>
                        <div class="form-group">
                            <label for="name">Correo electrónico</label>
                            <input type="email" name="email" class="form-control"
                                   placeholder="Correo electrónico" required="">
                        </div>
                        <div class="form-group">
                            <label for="name">Letra</label>
                            <input type="text" name="letra" class="form-control"
                                   placeholder="Letra" maxlength="2" required="">
                        </div>
                        <div class="form-group col-lg-3">
                            <label>Tipo de usuario</label>
                            <input class="" type="radio" id="operador" name="type" value="Operador" checked="">
                            <label for="operador">Operador</label><br> 
                            <input class="" type="radio"  id="supervisor" name="type" value="Supervisor">
                            <label for="supervisor">Supervisor</label>
                        </div>
                        <div class="form-group">
                            <label for="profile_image">Foto</label>
                            <input type="file" name="profile_image" id="profile_image" accept="image/*" class="form-control"
                                   placeholder="Foto de pérfil" required="">
                        </div>
                        <div class="form-group col-lg-3">
                            <button type="submit" class="btn btn-primary ">Guardar</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="box-footer">
            </div>
        </div>
    </div>
</div>

<script src="{{URL::to("admin_template/plugins/jQuery/jQuery-2.2.0.min.js")}}"></script>
<script>
$('#formAddUser').submit(function (event) {

    event.preventDefault();
    var data = new FormData(this);
    
    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        url: $('#url').val() + '/users/store',
        data: data,
        processData: false,
        contentType: false,
        cache: false,
        success: function (rta) {
            alertify.success("Se ha creado usuario " + rta);
            alert("Copie esta clave " +rta);
            $('#formAddUser')[0].reset();
        },
        error: function () {
            alertify.error("No se pudo crear el usuario, intente de nuevo");
        }
    });
});
</script>
@endsection