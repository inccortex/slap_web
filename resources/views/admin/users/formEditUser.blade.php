@extends("template.template")

@section("title")
    <h1>Crear nuevo Usuario</h1>
@endsection

@section("navigation")
    <li>
        <a href="{{URL::to('/')}}">
            <i class="fa fa-home"></i> Inicio
        </a>
    </li>
    <li>
        <i class="fa fa-plus"></i> Registrar Usuarios
    </li>
@endsection

@section("content")
    <div class="col-md-12">
        <div class="row">
            <div class="box">
                <div class="box-body">
                    <form role="form" method="POST" id="formEditUser" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="id" value="{{ encrypt($uEditar->id) }}">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="name">Nombres</label>
                                <input type="text" name="nombres" id="name" class="form-control"
                                       value="{{ $uEditar->nombres }}" required="">
                            </div>
                            <div class="form-group">
                                <label for="apellidos">Apellidos</label>
                                <input type="text" name="apellidos" id="apellidos" class="form-control"
                                       value="{{ $uEditar->apellidos }}" required="">
                            </div>
                            <div class="form-group">
                                <label for="username">Cédula</label>
                                <input type="text" name="user" id="username" min="1" class="form-control"
                                       value="{{ $uEditar->user }}" required="">
                            </div>
                            <div class="form-group">
                                <label for="name">Correo electrónico</label>
                                <input type="email" name="email" value="{{ $uEditar->email }}" class="form-control"
                                       required="">
                            </div>
                            <div class="form-group">
                                <label for="name">Letra</label>
                                <input type="text" name="letra" value="{{ $uEditar->letra }}" class="form-control"
                                       placeholder="Letra" maxlength="2" required="">
                            </div>
                            <div class="form-group">
                                <label for="">Foto de perfil</label>
                                <img src="{{ URL::to($uEditar->profile_image) }}" alt="" height="300" width="300">
                            </div>
                            <div class="form-group">
                                <label for="profile_image">Foto</label>
                                <input type="file" name="profile_image" id="profile_image" accept="image/*"
                                       class="form-control"
                                       placeholder="Foto de pérfil">
                            </div>
                            <div class="form-group">
                                <label for="name">Reiniciar UUID</label>
                                <select class="form-control" name="reinicio">
                                    <option value="0">NO</option>
                                    <option value="1">SI</option>
                                </select>
                            </div>
                            <div class="form-group col-lg-3">
                                <button type="submit" class="btn btn-primary ">Guardar</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="box-footer">
                </div>
            </div>
        </div>
    </div>

    <input type="hidden" id="url" value="{{URL::to('/')}}">
    <script src="{{URL::to("admin_template/plugins/jQuery/jQuery-2.2.0.min.js")}}"></script>
    <script>
        $('#formEditUser').submit(function (event) {

            event.preventDefault();
            var data = new FormData(this);

            $.ajax({
                type: "POST",
                enctype: 'multipart/form-data',
                url: $("#url").val() + '/users/update',
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                success: function (rta) {
                    location.href = $("#url").val() + "/users";
                    alertify.success("Se ha editado usuario " + rta);
                },
                error: function () {
                    alertify.error("No se pudo ediar el usuario, intente de nuevo");
                }
            });
        });
    </script>
@endsection