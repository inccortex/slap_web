@extends("template.template")

@section("title")
    <h1>Asingar Operdores a: {{$user->name}} {{$user->apellidos}}</h1>
@endsection

@section("navigation")
    <li>
        <a href="{{URL::to('/')}}">
            <i class="fa fa-home"></i> Inicio
        </a>
    </li>
    <li>
        <a href="{{URL::to('users')}}">
            <i class="fa fa-users"></i> Usuarios
        </a>
    </li>
    <li>
        <i class="fa fc-agenda-view"></i> Asignar Operadores
    </li>
@endsection

@section("content")
    <div class="col-md-12">
        <a href="{{URL::to('users')}}" class="btn btn-lg btn-warning pull-right">
            <i class="fa fa-mail-reply"></i> Volver
        </a>
    </div>
    <div class="col-md-6">
        <div class="row">
            <div class="box">
                <div class="box-header">
                    <h1 class="box-title">Operadores Asignados</h1>
                </div>
                <div class="box-body">
                    @if(count($opAssing) > 0)
                        <table class="table" id="opAssing">
                            <thead>
                            <tr>
                                <th>Img</th>
                                <th>Nombre</th>
                                <th>Documento</th>
                                <th>Deasasignar</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($opAssing as $opA)
                                <tr>
                                    <td><img src="{{URL::to($opA->profile_image)}}" style="width: 40px;"></td>
                                    <td>{{$opA->nombres}} {{$opA->apellidos}}</td>
                                    <td>{{$opA->username}}</td>
                                    <td id="{{$opA->id}}">
                                        <button class="btn btn-sm btn-danger" onclick="desAssignOp('{{$opA->id}}', '{{encrypt($opA->id)}}', '{{$id}}', '{{$opA->nombres}} {{$opA->apellidos}}', '{{$user->nombres}} {{$user->apellidos}}')">
                                            <i class="fa fa-close"></i>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        <span class="label label-warning">
                            No tiene operadores asignados
                        </span>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="row">
            <div class="box">
                <div class="box-header">
                    <h1 class="box-title">Operadores No Asignados</h1>
                </div>
                <div class="box-body">
                    @if(count($opNoAssing) > 0)
                        <table class="table" id="opNoAssing">
                            <thead>
                            <tr>
                                <th>Img</th>
                                <th>Nombre</th>
                                <th>Documento</th>
                                <th>Asignar</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($opNoAssing as $opA)
                                <tr>
                                    <td><img src="{{URL::to($opA->profile_image)}}" style="width: 40px;"></td>
                                    <td>{{$opA->nombres}} {{$opA->apellidos}}</td>
                                    <td>{{$opA->username}}</td>
                                    <td id="{{$opA->id}}">
                                        <button class="btn btn-sm btn-success" onclick="assignOp('{{$opA->id}}', '{{encrypt($opA->id)}}', '{{$id}}', '{{$opA->nombres}} {{$opA->apellidos}}', '{{$user->nombres}} {{$user->apellidos}}')">
                                            <i class="fa fa-plus"></i>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        <span class="label label-warning">
                            No existen más operadores por asignar
                        </span>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="url" value="{{URL::to('/')}}">
    {{ csrf_field() }}

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>

    <script>
        $(document).ready(function () {
            $('#opAssing').DataTable();
            $('#opNoAssing').DataTable();
        });
    </script>

    <script src="{{URL::to('js/users/assignOperator.js')}}"></script>
@endsection