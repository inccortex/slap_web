@extends("template.template")

@section("title")
    <h1>Inicio</h1>
@endsection

@section("navigation")
    <li>Inicio</li>
@endsection

@section("content")
    <div class="row">

        <div class="col-md-12">
            <!-- AREA CHART -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Indicadores Generales</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" onclick="traerInfo()">
                            <i class="fa  fa-refresh"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="chart">
                        <canvas id="areaChart" style="height: 400px; width: 400px;"></canvas>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->

        </div>

        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Conteos Generales</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" onclick="datosGeneralesOp()">
                            <i class="fa  fa-refresh"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">

                    <div class="col-md-6">
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title">Historicos</h3>
                                <div class="box-tools pull-right">
                                </div>
                            </div>
                            <div class="box-body" id="historicos">
                                Historico
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title">Registros Hoy</h3>
                                <div class="box-tools pull-right">
                                </div>
                            </div>
                            <div class="box-body" id="regHoy">
                                Hoy
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title">Registros Por Barrios</h3>
                                <div class="box-tools pull-right">
                                </div>
                            </div>
                            <div class="box-body" id="regBarrios">
                                barrios
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12" id="resultBorrar">

                    </div>

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>

        <div class="col-md-12">
            @if($user->puede_registrar == 1)
                <button type="button" class="btn btn-lg btn-danger btn-block" onclick="registroActividades('cerrar')">
                    <i class="fa fa-expeditedssl"></i> CERRAR REGISTRO DE ACTIVIDADES OPERADORES
                </button>
            @elseif($user->puede_registrar == 0)
                <button type="button" class="btn btn-lg btn-success btn-block" onclick="registroActividades('abrir')">
                    <i class="fa fa-check"></i> ABRIR REGISTRO DE ACTIVIDADES OPERADORES
                </button>
            @endif

            <!--<button type="button" class="btn btn-lg btn-warning btn-block" onclick="probarBorrar()">
                <i class="fa fa-expeditedssl"></i> BORRAR
            </button>-->
            <button type="button" class="btn btn-lg btn-warning btn-block" onclick="uploadAll()" id="btnupdate">
                <i class="fa fa-expeditedssl"></i> Editar Todos
            </button>
        </div>
    </div>
    <input type="hidden" id="url" value="{{URL::to('/')}}">
    {{ csrf_field() }}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
    <script src="{{URL::to('admin_template/plugins/chartjs/Chart.js')}}"></script>
    <script>

        $(document).ready(function () {
            traerInfo();
            datosGeneralesOp();
        });

        function traerInfo() {
            //console.log("Trayendo")
            $.ajax({
                method: "GET",
                url: $("#url").val() + "/datosGenerales",
                async: false
            }).done(function (result) {
                //console.log("RESULT - " + JSON.stringify(result))
                var datos = {
                    labels: [
                        'Luminarias: ' + result.luminarias,
                        'Faroles: ' + result.faroles,
                        'Reflectores: ' + result.reflectores,
                        'Terras: ' + result.terras,
                        'Transformadores: ' + result.trafos,
                        "Total Registrado: " + result.total,
                        "Meta General: 50000"
                    ],
                    datasets: [{
                        fillColor: "rgba(220,220,220,0.5)",
                        strokeColor: "rgba(220,220,220,0.8)",
                        highlightFill: "rgba(220,220,220,0.75)",
                        highlightStroke: "rgba(220,220,220,1)",
                        data: [
                            result.luminarias,
                            result.faroles,
                            result.reflectores,
                            result.terras,
                            result.trafos,
                            result.total,
                            50000
                        ]
                    }]
                };

                var canvas = document.getElementById("areaChart").getContext('2d')
                window.char = new Chart(canvas).Bar(datos)
            });
        }

        function datosGeneralesOp() {
            $.ajax({
                method: "GET",
                url: $("#url").val() + "/datosGeneralesOp",
                async: false
            }).done(function (result) {
                var tableBarrios = "<table class='table table-bordered' id='historicosBarrios'><thead><tr><th>Barrio</th><th>Cantidad</th></tr></thead><tbody>"
                var tableHistorico = "<table class='table table-bordered' id='historicosGenerales'><thead><tr><th>Usuario</th><th>Cantidad</th></tr></thead><tbody>"
                var tableHoy = "<table class='table table-bordered' id='registrosHoy'><thead><tr><th>Usuario</th><th>Cantidad</th></tr></thead><tbody>"

                $.each(result.historicos, function (key, item) {
                    tableHistorico += "<tr>"
                    tableHistorico += "<td>" + item.op + "</td>"
                    tableHistorico += "<td>" + item.cant + "</td>"
                    tableHistorico += "</tr>"
                })
                tableHistorico += "</tbody></table>"

                $.each(result.registrosHoy, function (key, item) {
                    tableHoy += "<tr>"
                    tableHoy += "<td>" + item.op + "</td>"
                    tableHoy += "<td>" + item.cant + "</td>"
                    tableHoy += "</tr>"
                })
                tableHoy += "</tbody></table>"

                $.each(result.historicosBarrios, function (key, item) {
                    tableBarrios += "<tr>"
                    tableBarrios += "<td>" + item.op + "</td>"
                    tableBarrios += "<td>" + item.cant + "</td>"
                    tableBarrios += "</tr>"
                })
                tableBarrios += "</tbody></table>"

                $("#historicos").html(tableHistorico)
                $("#regHoy").html(tableHoy)
                $("#regBarrios").html(tableBarrios)

            })

            $('#historicosGenerales').DataTable();
            $('#registrosHoy').DataTable();
            $('#historicosBarrios').DataTable();

        }

        function registroActividades(estado) {
            var est = ""
            var rut = ""
            if (estado === "abrir") {
                est = "ABRIR"
                rut = "abrirOperaciones"
            } else if (estado === "cerrar") {
                est = "CERRAR"
                rut = "cerrarOperaciones"
            }

            console.log($("#url").val() + "/" + rut)

            alertify
                .defaultValue("")
                .prompt("¿Está segur@ que desea " + est + " los registros para el operador desde el móvil? Tenga en cuenta que: <br/><br/> <ul><li>Se reiniciarán las metas diarias</li></ul> <br/><br/>Si es así, por favor escriba la aplabara CONFIRMAR",
                    function (val, ev) {
                        if (val) {
                            if (val === "CONFIRMAR") {
                                window.location.href = $("#url").val() + "/" + rut
                            } else {
                                alertify.error("Debe digitar la palabra correctamente para realizar la acción.")
                            }
                        } else {
                            alertify.error("Debe escribir la plabra CONFIRMAR en el campo de texto para permitir habilitar el usuario.")
                        }
                    }, function (ev) {
                        alertify.error("Se ha cancelado la acción.")
                    })
        }

        function probarBorrar() {
            var token = $("input[name*='_token']").val();
            var url = $("#url").val();
            $.ajax({
                method: "post",
                url: $("#url").val() + "/deleteActivos",
                data: {
                    _token: token
                },
                async: false
            }).done(function (result) {
                $("#resultBorrar").html(JSON.stringify(result))
            })
        }

        function uploadAll() {

            $("#btnupdate").attr('disabled', 'true');
            $("#btnupdate").html("Cargando");

            var token = $("input[name*='_token']").val();
            var url = $("#url").val();
            $.ajax({
                method: "post",
                url: $("#url").val() + "/uploadActivos",
                data: {
                    _token: token
                },
                async: false
            }).done(function (result) {
                alertify.log(JSON.stringify(result));
                $("#btnupdate").removeAttr('disabled');
                $("#btnupdate").html('<i class="fa fa-expeditedssl"></i> Editar Todos');
            })
        }

    </script>
@endsection