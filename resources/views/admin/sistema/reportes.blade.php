@extends("template.template")

@section("title")
    <h1>Reportes</h1>
@endsection

@section("navigation")
    <li>Reportes</li>
@endsection

@section("content")

    <style>

        #tableHead {
            background-color: #d7d6ce;
            text-align: center;
        }

        /*
                table tbody, table thead
                {
                    display: block;
                    //border:1px solid;
                }

                table tbody
                {
                    overflow: auto;
                    height: 500px;
                }

                th , td
                {
                    width: 50px;
                }*/

    </style>

    <div class="nav-tabs-custom col-md-12">
        <a href="{{ URL("generarKML") }}" class="btn btn-lg btn-danger pull-right">
            <i class="fa fa-map-marker"></i> Georreferenciacion
        </a>
        <ul class="nav nav-tabs">
            <li class="active">
                <a href="#registros" data-toggle="tab" aria-expanded="true">
                    <i class="fa fa-keyboard-o"></i> Registros
                </a>
            </li>
            <!--<li class="">
                <a href="#metas" data-toggle="tab" aria-expanded="false">
                    <i class="fa fa-male"></i> Metas
                </a>
            </li>-->
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="registros">

                <div class="row">
                    <div class="col-md-12" style="text-align: center;">
                        <h1>Registros realizados</h1>
                    </div>
                    <div class="col-md-12 control-group">
                        <form method="POST" action="{{ URL("excelRegistros") }}" id="FormularioExportacion">
                            <input type="hidden" id="contenido_excel" name="contenido"/>
                            {{ csrf_field() }}
                            <div class="controls col-md-10">
                                <div class="input-prepend">
                                <span class="add-on">
                                    <i class="icon-calendar"></i>
                                </span>
                                    <div class="form-group col-md-6">
                                        <label class="control-label">Fecha desde</label>
                                        <!--<input type="text" name="fechas" id="fechasRegistros" class=" m-ctrl-medium date-range form-control"/>-->
                                        <input type="date" name="fdesde" id="fdesde" class="form-control">

                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="control-label">Fecha hasta</label>
                                        <input type="date" name="fhasta" id="fhasta" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Filtro por Operador</label>
                                        <select class="form-control" id="operadorRegistros" name="idop">
                                            <option value="x">- TODOS -</option>
                                            @foreach($operadores  as $op)
                                                <option value="{{$op->id}}">{{$op->nombres}} {{$op->apellidos}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2" style="margin-top: 3%;">
                                <!--<button type="submit" id="btnGraficar" class="btn btn-success btn-block">
                                    <i class="fa fa-file-excel-o"></i> Generar Excel
                                </button>-->
                                <a onclick="imprimirExcel('malditaTabla')" class="btn btn-success btn-block">Exportar a
                                    Excel</a>
                                <button type="button" id="btnVerRegistros" class="btn btn-primary btn-block"
                                        onclick="verListadoRegistros('{{URL::to('/')}}')">
                                    <i class="fa fa-eye "></i> Ver Listado
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12" id="resultRegistros">
                        <div class="container">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#resultEi" data-toggle="tab" aria-expanded="true">
                                        <i class="fa fa-keyboard-o"></i> Elementos de Iluminacion
                                    </a>
                                </li>
                                <li class="">
                                    <a href="#resutlTrafo" data-toggle="tab" aria-expanded="false">
                                        <i class="fa fa-male"></i> Transformadores
                                    </a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="resultEi"
                                     style="overflow-x:scroll; width: 90%; height: 500px;">
                                    <div class="container" id="muestraEi">
                                        No se ha generado listado
                                    </div>
                                </div>
                                <div class="tab-pane" id="resutlTrafo"
                                     style="overflow-x:scroll; width: 90%; height: 500px;">
                                    <div class="container" id="muestraTrafo">
                                        No se ha generado listado
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="metas">
                <div class="row">
                    <div class="col-md-12" style="text-align: center;">
                        <h1>Metas generales</h1>
                    </div>
                    <div class="col-md-12 control-group">
                        <form method="POST" action="{{ URL("generarHC") }}">
                            {{ csrf_field() }}
                            <div class="controls col-md-10">
                                <div class="input-prepend">
                                <span class="add-on">
                                    <i class="icon-calendar"></i>
                                </span>
                                    <div class="form-group">
                                        <label class="control-label">Seleccione un rango de fechas</label>
                                        <input type="text" name="fechas"
                                               class=" m-ctrl-medium date-range form-control"/>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Filtro por Usuario</label>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="control-label">Operador</label>
                                        <select class="form-control">
                                            <option>No aplica</option>
                                            <option>Todos</option>
                                            <option>Operador X</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="control-label">Supervisor</label>
                                        <select class="form-control">
                                            <option>No aplica</option>
                                            <option>Todos</option>
                                            <option>Operador X</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2" style="margin-top: 5%;">
                                <button type="submit" id="btnGraficar" class="btn btn-success btn-block">
                                    <i class="fa fa-file-excel-o"></i> Generar Excel
                                </button>
                                <button type="button" id="btnVerMetas" class="btn btn-primary btn-block">
                                    <i class="fa fa-eye "></i> Ver Listado
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <input type="hidden" id="url" value="{{URL::to('/')}}">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fixed-header-table/1.3.0/jquery.fixedheadertable.min.js"></script>
    <script src="{{URL::to('js/reportes/lsitadoRegistros.js')}}"></script>
@endsection



