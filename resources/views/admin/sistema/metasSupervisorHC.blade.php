<table>
    <thead>
        <tr>
            <th>id</th>
            <th>Nombre</th>
            <th>Apellidos</th>
            <th>Cédula</th>
            <th>Realizado</th>
            <th>Meta de Hoy</th>
            <th>Meta global</th>
            <th>Historico</th>
        </tr>
    </thead>
    <tbody>
        @foreach($supervisores as $su)
        <tr>
            <td>{{ $su->id }}</td>
            <td>{{ $su->nombre }}</td>
            <td>{{ $su->apellido }}</td>
            <td>{{ $su->user }}</td>
            <td>{{ $su->metaSupervisor()->orderBy('fecha', 'DESC')->first() }}</td>
            <td>{{ $su->meta_hoy }}</td>
            <td>{{ $su->meta_global }}</td>
            <td>{{ $su->operarios()->with('activos')->count() }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
