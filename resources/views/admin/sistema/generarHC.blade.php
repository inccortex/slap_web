@extends("template.template")

@section("title")
<h1>Inicio</h1>
@endsection

@section("navigation")
<li>Inicio</li>
@endsection

@section("content")
<div class="row">
    <div class="col-md-4 col-sm-6 col-xs-12">
        <div class="info-box">
            <a href="{{ URL::to("generarHC") }}">
                <span class="info-box-icon bg-aqua"><i class="ion ion-ios-gear-outline"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">HOJA DE CÁLCULO</span>
                    <span class="info-box-number">{{ "sdf" }}<small></small></span>
                </div>
            </a>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-md-4 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-google-plus"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">GEOLOCALIZACIÓN</span>
                <span class="info-box-number">{{ "sdf" }}</span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <!-- fix for small devices only -->
    <div class="clearfix visible-sm-block"></div>
    <div class="col-md-4 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-green"><i class="ion ion-ios-cart-outline"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">VALIDACIONES PENDIENTES</span>
                <span class="info-box-number">{{ "sdf" }}</span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <div class="col-md-12">
    </div>
</div>
<input type="hidden" id="url" value="{{URL::to('/')}}">


@endsection