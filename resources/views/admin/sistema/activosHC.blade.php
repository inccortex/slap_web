<table class="table table-bordered">
    <thead>
        <tr>
            <th rowspan="4" id="tableHead">ID</th>
            <th rowspan="4" id="tableHead">CONSECUTIVO POSTE</th>
            <th rowspan="3" colspan="2" id="tableHead">COORDENADAS
                (Georreferenciación)
            </th>
            <th rowspan="4" id="tableHead">COMUNA</th>
            <th rowspan="4" id="tableHead">BARRIO</th>
            <th rowspan="4" id="tableHead">Numero de serie del elemento</th>
            <th colspan="11" id="tableHead">ELEMENTOS DE ILUMINACION</th>
            <th colspan="10" id="tableHead">APOYO</th>
            <th colspan="2" id="tableHead">BRAZO</th>
            <th colspan="3" id="tableHead">REDES</th>
            <th colspan="7" rowspan="3" id="tableHead">Novedades Luminaria</th>
            <th colspan="5" rowspan="3" id="tableHead">Novedades Apoyo</th>
            <th colspan="2" rowspan="3" id="tableHead">Novedades Brazo</th>
            <th colspan="2" rowspan="3" id="tableHead">Novedades Red</th>
            <th rowspan="4" id="tableHead">ENLACE FOTOS</th>
        </tr>
        <tr>

            <th colspan="6" id="tableHead">Luminarias</th>
            <th colspan="2" id="tableHead">Faroles</th>
            <th colspan="4" id="tableHead">Reflectores</th>
            <th colspan="5" id="tableHead">CENS</th>
            <th id="tableHead">Telecom</th>
            <th colspan="3" id="tableHead">Exclusivo AP</th>
            <th rowspan="3" id="tableHead">Corto 1.20 mts</th>
            <th rowspan="3" id="tableHead">Largo 2.40 mts</th>
            <th rowspan="4" id="tableHead">Cens</th>
            <th colspan="2" rowspan="2" id="tableHead">Exclusivos</th>
        </tr>
        <tr>
            <th rowspan="2" id="tableHead">Sodio</th>
            <th colspan="2" id="tableHead">led</th>
            <th rowspan="2" id="tableHead">Metal Halide</th>
            <th rowspan="2" id="tableHead">Mercurio</th>
            <th rowspan="2" id="tableHead">Luz Mixta</th>
            <th rowspan="2" id="tableHead">Sodio</th>
            <th rowspan="2" id="tableHead">Led</th>
            <th rowspan="2" id="tableHead">Sodio</th>
            <th rowspan="2" id="tableHead">Led</th>
            <th rowspan="2" id="tableHead">Metal Halide</th>
            <th rowspan="2" id="tableHead">Mercurio</th>
            <th rowspan="2" id="tableHead">Concreto</th>
            <th rowspan="2" id="tableHead">Metálico</th>
            <th rowspan="2" id="tableHead">Fibra</th>
            <th rowspan="2" id="tableHead">Riel</th>
            <th rowspan="2" id="tableHead">Madera</th>
            <th rowspan="2" id="tableHead">concreto</th>
            <th rowspan="2" id="tableHead">concreto</th>
            <th rowspan="2" id="tableHead">metalico</th>
            <th rowspan="2" id="tableHead">fibra</th>
        </tr>
        <tr>
            <th id="tableHead">Latitud</th>
            <th id="tableHead">longitud</th>
            <th id="tableHead">Tipo</th>
            <th id="tableHead">Pot.</th>
            <th id="tableHead">Aerea / Subterranea</th>
            <th id="tableHead">Distancia</th>
            <th id="tableHead">Tipo de elemento (Publico o Privado)</th>
            <th id="tableHead">Elemento de iluminación caído.</th>
            <th id="tableHead">Elemento de iluminación sin bombillo.</th>
            <th id="tableHead">Elemento de iluminación continuamente.</th>
            <th id="tableHead">Elemento de iluminación rota.</th>
            <th id="tableHead">Elemento de iluminación con bombillo expuesto al aire</th>
            <th id="tableHead">Necesario Poda</th>
            <th id="tableHead">Valla publicitaria</th>
            <th id="tableHead">Poste desplomado.</th>
            <th id="tableHead">Poste quebrado.</th>
            <th id="tableHead">Poste caído.</th>
            <th id="tableHead">Poste corroído.</th>
            <th id="tableHead">Brazo dañado o torcido</th>
            <th id="tableHead">Brazo corroido</th>
            <th id="tableHead">Red Descubierta</th>
            <th id="tableHead">Red Encauchetada</th>
        </tr>
    </thead>
</table>