<table>
    <thead>
        <tr>
            <th>id</th>
            <th>Nombre</th>
            <th>Apellidos</th>
            <th>Cédula</th>
            <th>Realizado</th>
            <th>Meta de Hoy</th>
            <th>Meta global</th>
            <th>Historico</th>
        </tr>
    </thead>
    <tbody>
        @foreach($operarios as $op)
        <tr>
            <td>{{ $op->id }}</td>
            <td>{{ $op->nombre }}</td>
            <td>{{ $op->apellido }}</td>
            <td>{{ $op->user }}</td>
            <td>{{ $op->metaOperador()->orderBy('fecha', 'DESC')->first() }}</td>
            <td>{{ $op->meta_hoy }}</td>
            <td>{{ $op->meta_global }}</td>
            <td>{{ $op->activos()->count() }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
