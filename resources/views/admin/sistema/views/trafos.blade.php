<html>
<style>
    #tableHead {
        background-color: #a9c9fb;
        text-align: center;
        border: 1px solid;
        border-color: #0c0c0c;
    }
</style>
<table>
    <thead>
    <tr>
        <th>{{$cont}}</th>
    </tr>
    <tr>
        <th rowspan="3" id="tableHead">ID</th>
        <th rowspan="3" id="tableHead">CONSECUTIVO POSTE</th>
        <th colspan="2" id="tableHead">COORDENADAS (Georreferenciación)</th>
        <th rowspan="3" id="tableHead">OPERADOR</th>
        <th rowspan="3" id="tableHead">FECHA REGISTRO</th>
        <th rowspan="3" id="tableHead">COMUNA</th>
        <th rowspan="3" id="tableHead">BARRIO</th>
        <th rowspan="3" id="tableHead">Numero de serie del elemento</th>
        <th colspan="9" id="tableHead">APOYO</th>
        <th rowspan="3" id="tableHead">Transformadores</th>
        <th colspan="6" id="tableHead">Novedades Apoyo</th>
        <th rowspan="3" id="tableHead">ENLACE FOTOS</th>
    </tr>
    <tr>
        <th></th>
        <th></th>
        <th rowspan="2" id="tableHead">Latitud</th>
        <th rowspan="2" id="tableHead">Longitud</th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th colspan="5" id="tableHead">CENS</th>
        <th id="tableHead">Telecom</th>
        <th colspan="3" id="tableHead">Exclusivo AP</th>
        <th></th>
        <th rowspan="2" id="tableHead">Valla publicitaria</th>
        <th rowspan="2" id="tableHead">Poste desplomado-></th>
        <th rowspan="2" id="tableHead">Poste desplomado-></th>
        <th rowspan="2" id="tableHead">Poste caído-></th>
        <th rowspan="2" id="tableHead">Poste corroído-></th>
        <th rowspan="2" id="tableHead">No aplica</th>
    </tr>
    <tr>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th id="tableHead">Concreto</th>
        <th id="tableHead">Metálico</th>
        <th id="tableHead">Fibra</th>
        <th id="tableHead">Riel</th>
        <th id="tableHead">Madera</th>
        <th id="tableHead">Concreto</th>
        <th id="tableHead">Concreto</th>
        <th id="tableHead">Metálico</th>
        <th id="tableHead">Fibra</th>
    </tr>
    </thead>
    <tbody>

    @foreach($trafos as $item)
        <tr>

            <td>{{$item->id}}</td>
            <td>{{$item->apid}}</td>
            <td>{{$item->latitud}}</td>
            <td>{{$item->longitud}}</td>
            <td>{{$item->nombres}} {{$item->apellidos}}</td>
            <td>{{$item->created_at}}</td>
            <td></td>
            <td></td>
            @if ($item->serial)
                <td>{{$item->serial}}</td>
            @else
                <td></td>
            @endif

        <!--APOYO-->
            @if ($item->apPertenece == 1)
                @if ($item->apTipo == 1)
                    <td>{{$item->apLong}}</td>
                @else
                    <td></td>
                @endif
                @if ($item->apTipo == 2)
                    <td>{{$item->apLong}}</td>
                @else
                    <td></td>
                @endif
                @if ($item->apTipo == 3)
                    <td>{{$item->apLong}}</td>
                @else
                    <td></td>
                @endif
                @if ($item->apTipo == 4)
                    <td>{{$item->apLong}}</td>
                @else
                    <td></td>
                @endif
                @if ($item->apTipo == 5)
                    <td>$item->apLong</td>
                @else
                    <td></td>
                @endif
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            @endif

            @if ($item->apPertenece == 2)
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>{{$item->apLong}}</td>
                <td></td>
                <td></td>
                <td></td>
            @endif
            @if ($item->apPertenece == 3)
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>

                @if ($item->apTipo == 1)
                    <td>{{$item->apLong}}</td>
                @else
                    <td></td>
                @endif
                @if ($item->apTipo == 2)
                    <td>{{$item->apLong}}</td>
                @else
                    <td></td>
                @endif
                @if ($item->apTipo == 3)
                    <td>{{$item->apLong}}</td>
                @else
                    <td></td>
                @endif
            @endif
            @if ($item->apPertenece == 0)
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            @endif

            <td>{{$item->potencia}}</td>

            <!--NOVEDADES Apoyo-->
            @if ($item->apNovedades)
                @if (strpos($item->apNovedades, "Valla publicitaria") !== FALSE)
                    <td>x</td>
                @else
                    <td></td>
                @endif
                @if (strpos($item->apNovedades, "Poste desplomado") !== FALSE)
                    <td>x</td>
                @else
                    <td></td>
                @endif
                @if (strpos($item->apNovedades, "Poste quebrado") !== FALSE)
                    <td>x</td>
                @else
                    <td></td>
                @endif
                @if (strpos($item->apNovedades, "Poste caído") !== FALSE)
                    <td>x</td>
                @else
                    <td></td>
                @endif
                @if (strpos($item->apNovedades, "Poste corroído") !== FALSE)
                    <td>x</td>
                @else
                    <td></td>
                @endif
                @if (strpos($item->apNovedades, "No aplica") !== FALSE)
                    <td>x</td>
                @elseif(strpos($item->apNovedades,"NO APLICA - REFLECTOR") !== FALSE) {
                <td>x</td>
                @else
                    <td></td>
                @endif
            @else
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>x</td>
            @endif

            <td><a href="{{URL::to('storage/fotos/'.$item->foto)}}" target="_blank">Ver foto</a></td>
    @endforeach

    </tbody>
</table>
</html>