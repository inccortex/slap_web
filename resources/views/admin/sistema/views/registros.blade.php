<html>

<style>
    #tableHead {
        background-color: #a9c9fb;
        text-align: center;
        border: 1px solid;
        border-color: #0c0c0c;
    }
</style>

<table>
    <thead>
    <tr>
        <th rowspan="4" id="tableHead">ID</th>
        <th rowspan="4" id="tableHead">CONSECUTIVO POSTE</th>
        <th rowspan="4" id="tableHead">DIRECCION</th>
        <th rowspan="4" id="tableHead">NOMENCLATURA</th>
        <th rowspan="4" id="tableHead">BARRIO</th>
        <th rowspan="4" id="tableHead">COMUNA</th>
        <th colspan="2" id="tableHead">COORDENADAS (Georreferenciación)</th>
        <th rowspan="4" id="tableHead">OPERADOR</th>
        <th rowspan="4" id="tableHead">FECHA REGISTRO</th>
        <th rowspan="4" id="tableHead">HORA</th>
        <th rowspan="4" id="tableHead">Numero de serie del elemento</th>
        <th colspan="15" id="tableHead">ELEMENTOS DE ILUMINACION</th>
        <th colspan="10" id="tableHead">APOYO</th>
        <th colspan="3" id="tableHead">BRAZO</th>
        <th colspan="4" id="tableHead">REDES</th>
        <th colspan="8" id="tableHead">Novedades Luminaria</th>
        <th colspan="6" id="tableHead">Novedades Apoyo</th>
        <th colspan="3" id="tableHead">Novedades Brazo</th>
        <th colspan="3" id="tableHead">Novedades Red</th>
        <th rowspan="4" id="tableHead">ENLACE FOTOS</th>
    </tr>
    <tr>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th rowspan="3" id="tableHead">Longitud</th>
        <th rowspan="3" id="tableHead">Latitud</th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th colspan="6" id="tableHead">Luminarias</th>
        <th colspan="2" id="tableHead">Faroles</th>
        <th colspan="4" id="tableHead">Reflectores</th>
        <th colspan="2" id="tableHead">Terra</th>
        <th rowspan="3" id="tableHead">Observacion</th>
        <th colspan="5" id="tableHead">CENS</th>
        <th id="tableHead">Telecom</th>
        <th colspan="3" id="tableHead">Exclusivo AP</th>
        <th rowspan="3" id="tableHead">Observacion</th>
        <th rowspan="3" id="tableHead">Corto 1.20 mts</th>
        <th rowspan="3" id="tableHead">Largo 2.40 mts</th>
        <th rowspan="3" id="tableHead">Observacion</th>
        <th rowspan="3" id="tableHead">Cens</th>
        <th colspan="2" id="tableHead">Exclusivos</th>
        <th rowspan="3" id="tableHead">Observacion</th>
        <th rowspan="3" id="tableHead">Elemento (Publico o Privado)</th>
        <th rowspan="3" id="tableHead">Iluminación caída</th>
        <th rowspan="3" id="tableHead">Sin bombillo</th>
        <th rowspan="3" id="tableHead">Directa</th>
        <th rowspan="3" id="tableHead">Elemento roto</th>
        <th rowspan="3" id="tableHead">Bombillo expuesto</th>
        <th rowspan="3" id="tableHead">Necesario Poda</th>
        <th rowspan="3" id="tableHead">No aplica</th>
        <th rowspan="3" id="tableHead">Valla publicitaria</th>
        <th rowspan="3" id="tableHead">Poste desplomado</th>
        <th rowspan="3" id="tableHead">Poste quebrado</th>
        <th rowspan="3" id="tableHead">Poste caído</th>
        <th rowspan="3" id="tableHead">Poste corroído</th>
        <th rowspan="3" id="tableHead">No aplica</th>
        <th rowspan="3" id="tableHead">Brazo dañado o torcido</th>
        <th rowspan="3" id="tableHead">Brazo corroido</th>
        <th rowspan="3" id="tableHead">No aplica</th>
        <th rowspan="3" id="tableHead">Red Descubierta</th>
        <th rowspan="3" id="tableHead">Red Encauchetada</th>
        <th rowspan="3" id="tableHead">No aplica</th>
    </tr>
    <tr>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th rowspan="2" id="tableHead">Sodio</th>
        <th colspan="2" id="tableHead">led</th>
        <th rowspan="2" id="tableHead">Metal Halide</th>
        <th rowspan="2" id="tableHead">Mercurio</th>
        <th rowspan="2" id="tableHead">Luz Mixta</th>
        <th rowspan="2" id="tableHead">Sodio</th>
        <th rowspan="2" id="tableHead">Led</th>
        <th rowspan="2" id="tableHead">Sodio</th>
        <th rowspan="2" id="tableHead">Led</th>
        <th rowspan="2" id="tableHead">Metal Halide</th>
        <th rowspan="2" id="tableHead">Mercurio</th>
        <th rowspan="2" id="tableHead">Sodio</th>
        <th rowspan="2" id="tableHead">Led</th>
        <th></th>
        <th rowspan="2" id="tableHead">Concreto</th>
        <th rowspan="2" id="tableHead">Metálico</th>
        <th rowspan="2" id="tableHead">Fibra</th>
        <th rowspan="2" id="tableHead">Riel</th>
        <th rowspan="2" id="tableHead">Madera</th>
        <th rowspan="2" id="tableHead">concreto</th>
        <th rowspan="2" id="tableHead">concreto</th>
        <th rowspan="2" id="tableHead">metalico</th>
        <th rowspan="2" id="tableHead">fibra</th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th rowspan="2" id="tableHead">Aerea / Subterranea</th>
        <th rowspan="2" id="tableHead">Distancia</th>
    </tr>
    <tr>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th id="tableHead">Tipo</th>
        <th id="tableHead">Pot</th>
        <!--<th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>-->

    </tr>
    </thead>
    <tbody>

    @foreach($luminarias as $item)

        <!-- INICIO DE LUMINARIAS REFLECTOR -->

        <tr>
            <td>{{$item->eiid}}</td>
            <td>{{$item->apid}}</td>
            <td>{{$item->direccion}}</td>
            <td>{{$item->nomenclatura}}</td>
            <td>{{$item->barrio}}</td>
            <td></td>
            <td>{{$item->latitud}}</td>
            <td>{{$item->longitud}}</td>
            <td>{{$item->nombres}} {{$item->apellidos}}</td>
            <td>{{$item->fecha_registro}}</td>
            <td>{{$item->hora}}</td>

            @if ($item->serial)
                <td>{{$item->serial}}</td>
            @else
                <td></td>
            @endif
        <!--ELEMENTO DE ILUMINACION LUMINARIA REFLECTOR-->
            @if ($item->tipo == 1)

                @if ($item->tecnologia)
                    @if ($item->tecnologia == "sodio")
                        <td>{{$item->nombre}} {{$item->potencia}}</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>{{$item->ilmObservacion}}</td>
                    @endif
                    @if ($item->tecnologia == "led")
                        <td></td>
                        <td>{{$item->nombre}}</td>
                        <td>{{$item->potencia}}</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>{{$item->ilmObservacion}}</td>
                    @endif
                    @if ($item->tecnologia == "metal halide")
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>{{$item->potencia}}</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>{{$item->ilmObservacion}}</td>
                    @endif
                    @if ($item->tecnologia == "mercurio")
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>{{$item->potencia}}</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>{{$item->ilmObservacion}}</td>
                    @endif
                    @if ($item->tecnologia == "luz mixta")
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>{{$item->potencia}}</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>{{$item->ilmObservacion}}</td>
                    @endif
                @else
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                @endif
            @endif
            @if ($item->tipo == 3)

                @if ($item->tecnologia)
                    @if ($item->tecnologia == "sodio")
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>{{$item->potencia}}</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>{{$item->ilmObservacion}}</td>
                    @endif
                    @if ($item->tecnologia == "led")
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>{{$item->potencia}}</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>{{$item->ilmObservacion}}</td>
                    @endif
                    @if ($item->tecnologia == "metal halide")
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>{{$item->potencia}}</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>{{$item->ilmObservacion}}</td>
                    @endif
                    @if ($item->tecnologia == "mercurio")
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>{{$item->potencia}}</td>
                        <td></td>
                        <td></td>
                        <td>{{$item->ilmObservacion}}</td>
                    @endif
                @else
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                @endif
            @endif
        <!--APOYO-->
            @if ($item->apPertence == 1)
                @if ($item->apTipo == 1)
                    <td>{{$item->apLong}}</td>
                @else
                    <td></td>
                @endif
                @if ($item->apTipo == 2)
                    <td>{{$item->apLong}}</td>
                @else
                    <td></td>
                @endif
                @if ($item->apTipo == 3)
                    <td>{{$item->apLong}}</td>
                @else
                    <td></td>
                @endif
                @if ($item->apTipo == 4)
                    <td>{{$item->apLong}}</td>
                @else
                    <td></td>
                @endif
                @if ($item->apTipo == 5)
                    <td>{{$item->apLong}}</td>
                @else
                    <td></td>
                @endif
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>{{$item->apObservacion}}</td>
            @endif
            @if ($item->apPertence == 2)
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>{{$item->apLong}}</td>
                <td></td>
                <td></td>
                <td></td>
                <td>{{$item->apObservacion}}</td>
            @endif
            @if ($item->apPertence == 3)
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                @if ($item->apTipo == 1)
                    <td>{{$item->apLong}}</td>
                @else
                    <td></td>
                @endif
                @if ($item->apTipo == 2)
                    <td>{{$item->apLong}}</td>
                @else
                    <td></td>
                @endif
                @if ($item->apTipo == 3)
                    <td>{{$item->apLong}}</td>
                @else
                    <td></td>
                @endif
                <td>{{$item->apObservacion}}</td>
            @endif
            @if ($item->apPertence == 0)
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            @endif
        <!--BBRAZO-->

            @if ($item->brzTipo == 1)
                @if($item->incorporado == 0)
                    <td>Incorporado</td>
                @elseif($item->incorporado == 1)
                    <td>No Incorporado</td>
                @else
                    <td></td>
                @endif
                <td></td>
                <td>{{$item->brzObservacion}}</td>
            @endif
            @if ($item->brzTipo == 2)
                <td></td>
                @if($item->incorporado == 0)
                    <td>Incorporado</td>
                @elseif($item->incorporado == 1)
                    <td>No Incorporado</td>
                @else
                    <td></td>
                @endif
                <td>{{$item->brzObservacion}}</td>
            @endif
            @if ($item->brzTipo == 3)
                <td></td>
                <td></td>
                <td></td>
            @endif

        <!--RED-->
            @if ($item->redTipo == 1)
                <td>x</td>
                <td></td>
                <td></td>
                <td>{{$item->redObservacion}}</td>
            @endif
            @if ($item->redTipo == 2)
                <td></td>
                @if ($item->redAerea == 1)
                    <td>Aerea</td>
                @elseif($item->redAerea == 0)
                    <td>Subterranea</td>
                @else
                    <td></td>
                @endif
                <td>{{$item->redDistancia}}</td>
                <td>{{$item->redObservacion}}</td>
            @endif
        <!--NOVEDADES LUMINARIA-->
            @if ($item->lumNovedades)

                @if (strpos($item->lumNovedades, "Elemento Público") !== FALSE)
                    <td>Elemento Público</td>
                @elseif(strpos($item->lumNovedades, "Elemento Privado") !== FALSE)
                    <td>Elemento Privado</td>
                @else
                    <td>Elemento Público</td>
                @endif
                @if (strpos($item->lumNovedades, "Iluminación caída") !== FALSE)
                    <td>x</td>
                @else
                    <td></td>
                @endif
                @if (strpos($item->lumNovedades, "Sin bombillo") !== FALSE)
                    <td>x</td>
                @else
                    <td></td>
                @endif
                @if (strpos($item->lumNovedades, "Directa") !== FALSE)
                    <td>x</td>
                @else
                    <td></td>
                @endif
                @if (strpos($item->lumNovedades, "Elemento roto") !== FALSE)
                    <td>x</td>
                @else
                    <td></td>
                @endif
                @if (strpos($item->lumNovedades, "Bombillo expuesto") !== FALSE)
                <td>x</td>
                @else
                    <td></td>
                @endif
                @if (strpos($item->lumNovedades, "Necesario Poda") !== FALSE)
                    <td>x</td>
                @else
                    <td></td>
                @endif
                @if (strpos($item->lumNovedades, "No aplica") !== FALSE)
                    <td>x</td>
                @else
                    <td></td>
                @endif
            @else
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>x</td>
            @endif

        <!--NOVEDADES Apoyo-->
            @if ($item->apNovedades)
                @if (strpos($item->apNovedades, "Valla publicitaria") !== FALSE)
                    <td>x</td>
                @else
                    <td></td>
                @endif
                @if (strpos($item->apNovedades, "Poste desplomado") !== FALSE)
                    <td>x</td>
                @else
                    <td></td>
                @endif
                @if (strpos($item->apNovedades,"Poste quebrado") !== FALSE)
                    <td>x</td>
                @else
                    <td></td>
                @endif
                @if (strpos($item->apNovedades,"Poste caído") !== FALSE)
                    <td>x</td>
                @else
                    <td></td>
                @endif
                @if (strpos($item->apNovedades,"Poste corroído") !== FALSE)
                    <td>x</td>
                @else
                    <td></td>
                @endif
                @if (strpos($item->apNovedades,"No aplica") !== FALSE)
                    <td>x</td>
                @elseif (strpos($item->apNovedades,"NO APLICA - REFLECTOR") !== FALSE)
                    <td>x</td>
                @else
                    <td></td>
                @endif
            @else
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>x</td>
            @endif
        <!--Novedades del brazo-->
            @if ($item->brzNovedades)
                @if (strpos($item->brzNovedades, "Brazo dañado o torcido") !== FALSE)
                    <td>x</td>
                @else
                    <td></td>
                @endif
                @if (strpos($item->brzNovedades, "Brazo corroído") !== FALSE)
                    <td>x</td>
                @else
                    <td></td>
                @endif
                @if (strpos($item->brzNovedades,"No aplica") !== FALSE)
                    <td>x</td>
                @elseif(strpos($item->brzNovedades,"NO APLICA - REFLECTOR") !== FALSE)
                    <td>x</td>
                @else
                    <td></td>
                @endif
            @else
                <td></td>
                <td></td>
                <td>x</td>
            @endif
        <!--Novedades RED-->
            @if ($item->redNovedades)
                @if ($item->redNovedades == "Red Abierta")
                    <td>x</td>
                @else
                    <td></td>
                @endif
                @if ($item->redNovedades == "Red Encauchetada")
                    <td>x</td>
                @else
                    <td></td>
                @endif
                @if ($item->redNovedades == "No aplica")
                    <td>x</td>
                @else
                    <td></td>
                @endif
            @else
                <td></td>
                <td></td>
                <td>x</td>
        @endif

        <!--FOTO-->
            <td><a href="{{URL::to('storage/fotos/'.$item->foto)}}"
                   target="_blank">{{URL::to('storage/fotos/'.$item->foto)}}</a></td>
        </tr>
        <!-- FIN LUMINARIAS REFLECTOR -->

    @endforeach

    @foreach($farolest as $item)

        <!-- INICIO FAROL TERRA -->

        <tr>

            <td>{{$item->eiid}}</td>
            <td></td>
            <td>{{$item->direccion}}</td>
            <td>{{$item->nomenclatura}}</td>
            <td>{{$item->barrio}}</td>
            <td></td>
            <td>{{$item->latitud}}</td>
            <td>{{$item->longitud}}</td>
            <td>{{$item->nombres}} {{$item->apellidos}}</td>
            <td>{{$item->fecha_registro}}</td>
            <td>{{$item->hora}}</td>
            @if ($item->serial)
                <td>{{$item->serial}}</td>
            @else
                <td></td>
            @endif

        <!--ELEMENTO DE ILUMINACION FAROL TERRA-->
            @if ($item->tipo == 2)
                @if ($item->tecnologia)
                    @if ($item->tecnologia == "sodio")
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>{{$item->potencia}}</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>{{$item->ilumObservacion}}</td>
                    @endif
                    @if ($item->tecnologia == "led")
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>{{$item->potencia}}</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>{{$item->ilumObservacion}}</td>
                    @endif
                @else
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                @endif
            @endif

            @if($item->tipo == 4)

                @if ($item->tecnologia)
                    @if ($item->tecnologia == "sodio")
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>{{$item->potencia}}</td>
                        <td></td>
                        <td>{{$item->ilumObservacion}}</td>
                    @endif
                    @if ($item->tecnologia == "led")
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>{{$item->potencia}}</td>
                        <td>{{$item->ilumObservacion}}</td>
                    @endif
                @else
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                @endif
            @endif

        <!--apoyo y brazo-->
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>

            <!--RED-->
            @if ($item->redTipo == 1)
                <td>x</td>
                <td></td>
                <td></td>
                <td>{{$item->redObservacion}}</td>
            @endif
            @if ($item->redTipo == 2)
                <td></td>
                @if ($item->redAerea == 1)
                    <td>Aerea</td>
                @elseif($item->redAerea == 0)
                    <td>Subterranea</td>
                @else
                    <td></td>
                @endif
                <td>{{$item->redDistancia}}</td>
                <td>{{$item->redObservacion}}</td>
            @endif

        <!--NOVEDADES LUMINARIA-->
            @if ($item->lumNovedades)
                @if (strpos($item->lumNovedades,"Elemento Público") !== FALSE)
                    <td>Elemento Público</td>
                @elseif(strpos($item->lumNovedades,"Elemento Privado") !== FALSE)
                    <td>Elemento Privado</td>
                @else
                    <td></td>
                @endif
                @if (strpos($item->lumNovedades,"Iluminación caída") !== FALSE)
                    <td>x</td>
                @else
                    <td></td>
                @endif
                @if (strpos($item->lumNovedades,"Sin bombillo") !== FALSE)
                    <td>x</td>
                @else
                    <td></td>
                @endif
                @if (strpos($item->lumNovedades,"Directa") !== FALSE)
                    <td>x</td>
                @else
                    <td></td>
                @endif
                @if (strpos($item->lumNovedades,"Elemento roto") !== FALSE)
                    <td>x</td>
                @else
                    <td></td>
                @endif
                @if (strpos($item->lumNovedades,"Bombillo expuesto") !== FALSE)
                    <td>x</td>
                @else
                    <td></td>
                @endif
                @if (strpos($item->lumNovedades,"Necesario Poda") !== FALSE)
                    <td>x</td>
                @else
                    <td></td>
                @endif
                @if (strpos($item->lumNovedades,"No aplica") !== FALSE)
                    <td>x</td>
                @else
                    <td></td>
                @endif
            @else
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>x</td>
            @endif


        <!---NOVEDADES Apoyo-->
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>


            <!--Novedades del brazo-->
            <td></td>
            <td></td>
            <td></td>


            <!--Novedades RED-->
            @if ($item->redNovedades)
                @if ($item->redNovedades == "Red Descubierta")
                    <td>x</td>
                @else
                    <td></td>
                @endif
                @if ($item->redNovedades == "Red Encauchetada")
                    <td>x</td>
                @else
                    <td></td>
                @endif
                @if ($item->redNovedades == "No aplica")
                    <td>x</td>
                @else
                    <td></td>
                @endif
            @else
                <td></td>
                <td></td>
                <td>x</td>
        @endif

        <!--FOTO-->
            <td><a href="{{URL::to('storage/fotos/'.$item->foto)}}"
                   target="_blank">{{URL::to('storage/fotos/'.$item->foto)}}</a></td>

        </tr>

        <!-- FIN FAROL TERRA -->

    @endforeach

    </tbody>
</table>
</html>
