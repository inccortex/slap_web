@extends("template.template")

@section("content")

    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                Nuevas direciones
            </div>
            <div class="box-body">
                <div class="col-md-12">
                    <button class="btn btn-block btn-primary" id="btnGenerar" onclick="update()">
                        <i class="fa fa-send"></i> Generar
                    </button>
                </div>
                <div class="col-md-12" id="muestraResult">

                </div>
            </div>
        </div>
    </div>

    <input type="hidden" id="url" value="{{URL::to('/')}}">
    {{ csrf_field() }}

    <script>
        function generar() {
            var res = ""
            $("#btnGenerar").attr("disabled", "true")
            $.ajax({
                method: "get",
                url: $("#url").val() + "/getActivos",
                data: {},
                async: false
            }).done(function (result) {
                console.log(result.length)

                //var address = getAddress(result[0].id, result[0].latitud, result[0].longitud)
                var tam = result.length
                if (result.length > 0) {
                    alertify.success("Se van a procesar - " + tam + " registros, esto puede tardar varios minutos, por favor espere")
                    var i = 1
                    //getAddress(0, '7.9145613', '-72.493595')
                    $.each(result, function (ix, item) {
                     //console.log("viendo el registro: " + item.id)
                     var address = getAddress(item.id, item.latitud, item.longitud)
                     res += "<p>#: " + i + ", ID: " + item.id + ", Direccion Actual: " + item.direccion + ", Direccion nueva: " + address + "</p><br/>"
                     //console.log(res)
                     updateAddres(item.id, address[0], address[1])
                     i++
                     })

                } else {
                    alertify.error("No se encuentran registros en la consulta.")
                }

            }).error(function (err) {
                console.log("Error")
            })
            $("#muestraResult").html("<h2>Se han actualizado los siguientes registros:</h2> <br/> " + res)
        }

        function getAddress(id, lat, long) {
            var dir = []
            lar = "7.9145613"
            log = "-72.493595"
            $.ajax({
                method: "get",
                url: "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + lat + "," + long + "&key=AIzaSyD_D3OQg95OYPpvFd5CCYxkJ7MZ6ygnw8E",
                data: {},
                async: false
            }).done(function (result) {
                dir[0] = result.results[1].address_components[0].long_name
                dir[1] = result.results[0].formatted_address.split(",")[0]
                console.log("BARRIO: " + dir[0] + " NUEVA DIRECCION: " + dir[1] + " COMPLETA: " + result.results[0].formatted_address)
            }).error(function (err) {
                console.log("Error")
            })
            return dir
        }

        function updateAddres(id, barrio, direccion) {
            var token = $("input[name*='_token']").val();
            $.ajax({
                method: "post",
                url: $("#url").val() + "/updateAddress",
                data: {
                    id: id,
                    direccion: direccion,
                    barrio: barrio,
                    _token: token
                },
                async: false
            }).done(function (result) {
                console.log(JSON.stringify(result))
                return
            }).error(function (err) {
                console.log("Error")
            })
        }

        function update() {
            var token = $("input[name*='_token']").val();
            $.ajax({
                method: "post",
                url: $("#url").val() + "/updateAddress",
                data: {
                    _token: token
                },
                async: false
            }).done(function (result) {
                console.log(JSON.stringify(result))
                return
            }).error(function (err) {
                console.log("Error")
            })
        }


    </script>

@endsection