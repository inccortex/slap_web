<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activo extends Model
{

    protected $table = 'activo';
    protected $fillable = ['latitud', 'longitud', 'fecha_registro', 'hora', 'users_id', 'direccion', 'nomenclatura', 'barrio', 'comuna'];

    public function usuario()
    {
        return $this->belongsTo('App\User');
    }

    public function apoyo()
    {
        return $this->hasOne('App\Apoyo');
    }

    public function red()
    {
        return $this->hasOne('App\Red');
    }

    public function luminarias()
    {
        return $this->hasMany('App\ElementoIluminacion')->where('tipo', 1);
    }

    public function faroles()
    {
        return $this->hasMany('App\ElementoIluminacion')->where('tipo', 2);
    }

    public function reflectores()
    {
        return $this->hasMany('App\ElementoIluminacion')->where('tipo', 3);
    }

    public function terreos()
    {
        return $this->hasMany('App\ElementoIluminacion')->where('tipo', 4);
    }
}
