<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Red extends Model {

    protected $table = 'red';
    protected $fillable = ['tipo', 'v_tipo', 'distancia', 'v_distancia','aerea', 
        'observacion', 'novedades', 'activo_id'];

    public function activo() {
        return $this->belongsTo('App\Activo');
    }
}
