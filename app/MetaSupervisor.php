<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class MetaSupervisor extends Model
{
    protected $table = 'meta_supervisor';
}