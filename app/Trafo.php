<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trafo extends Model {

    protected $table = 'trafo';
    protected $fillable = ['potencia', 'latitud', 'longitud', 'observacion', 'serial', 'users_id'];

    public function usuario() {
        return $this->belongsTo('App\User');
    }

    public function apoyos() {
        return $this->hasMany('App\Apoyo');
    }
}
