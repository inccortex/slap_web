<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brazo extends Model {

    protected $table = 'brazo';
    protected $fillable = ['tipo', 'v_tipo', 'incorporado', 'observacion', 'novedades', 'iluminacion_id'];
    
    public function iluminacion(){
        return $this->hasOne('App\Iluminacion');        
    }
}
