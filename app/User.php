<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable {

    use Notifiable;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombres', 'apellidos', 'user', 'type', 'email', 'letra', 'profile_image', 'uuid', 'password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * 
     * @return type
     */
    public function supervisor() {
        return $this->belongsTo(User::class, 'supervisor_id');
    }

    /**
     * @return type
     */
    public function operarios() {
        return $this->hasMany(User::class, 'supervisor_id');
    }

    public function trafos() {
        return $this->hasMany('App\Trafo', 'users_id');
    }
    
    public function activos() {
        return $this->hasMany('App\Activo', 'users_id');
    }
    
    public function apoyos() {
        return $this->hasManyThrough('App\Apoyo', 'App\Activo', 'users_id');
    }
    
    public function redes() {
        return $this->hasManyThrough('App\Red', 'App\Activo', 'users_id');
    }
    
    public function brazos() {
        return $this->hasManyThrough('App\Brazo', 'App\Activo', 'users_id');
    }
    
    public function iluminacion() {
        return $this->apoyos()->with('iluminacion');
    }
    
    public function metaOperador(){
        return $this->hasMany('App\MetaOperador', 'users_id');
    }
    
    public function metaSupervisor(){
        return $this->hasMany('App\MetaSupervisor', 'users_id');
    }
    
    public function activosDeSuper(){
        return $this->hasManyThrough('App\Activo', 'App\User', 'supervisor_id', 'users_id');
    }
    
//    public function scopeHistoricoSupervisor(){
//        $historico = 0;
//        $this->operarios->each(function($value) use (&$historico){
//            $historico += $value->activos()->count();
//        });
//        return $historico;
//    }
}
