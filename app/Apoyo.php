<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Apoyo extends Model {

    protected $table = 'apoyo';
    protected $fillable = ['pertenece', 'v_pertence', 'tipo', 'v_tipo',
        'longitud', 'v_longitud', 'observacion', 'novedades', 'activo_id', 'trafo_id'];

    public function activo() {
        return $this->belongsTo('App\Activo');
    }

    public function trafo() {
        return $this->belongsTo('App\Trafo');
    }
}
