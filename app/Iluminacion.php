<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Iluminacion extends Model {

    protected $table = 'iluminacion';
    protected $fillable = ['tipo', 'tecnologia', 'v_tecnologia', 'nombre',
        'potencia', 'v_potencia', 'observacion', 'novedades', 'serial', 'activo_id'];

    public function activo() {
        return $this->belongsTo('App\Activo');
    }
    
    public function brazo(){
        return $this->hasOne('App\Brazo');
    }

}
