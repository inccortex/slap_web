<?php

namespace App\Http\Controllers;

use App\Activo;
use App\Apoyo;
use App\Brazo;
use App\Iluminacion;
use App\Red;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Excel;
use Illuminate\Support\Facades\Input;
use Mockery\Exception;
use PhpParser\InvalidTokenLexer;

class ActivoController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $nuevo = Activo::create($request->all());

        if ($nuevo) {
            return response()->json(['state' => 'ok', 'message' => 'Se registró el activo', 'id' => encrypt($nuevo->id)]);
        }
        return response()->json(['estate' => 'error', 'message' => 'No se registró el activo', 'id' => -1]);
    }

    /**
     * Método que regresa guarda las novedades reportadas a un activo
     * @param Request $request con el id del activo y un arreglo de codigo de novedades
     */
    public function addNovedad(Request $request)
    {

        $novedad = "";
        foreach ($request->input('novedades') as $novedades) {

            switch ($novedades) {
                case 0:
                    $novedad += "Luminaria caída, ";
                    break;
                case 1:
                    $novedad += "Luminaria sin bombillo, ";
                    break;
                case 2:
                    $novedad += "Luminaria encendida continuamente, ";
                    break;
                case 3:
                    $novedad += "Luminaria rota, ";
                    break;
                case 4:
                    $novedad += "Valla publicitaria, ";
                    break;
                case 5:
                    $novedad += "Poste desplomado, ";
                    break;
                case 6:
                    $novedad += "Poste quebrado, ";
                    break;
                case 7:
                    $novedad += "Poste caído, ";
                    break;
                case 8:
                    $novedad += "Poste corroído, ";
                    break;
                case 9:
                    $novedad += "Tramos en el suelo, ";
                    break;
                case 10:
                    $novedad += "Transformador con fuga de aceite, ";
                    break;
                case 11:
                    $novedad += "No aplica, ";
                    break;
            }
        }
        // obligatoriamente acá debo capturar la excepcion???
        $activo = Activo::findOrFail($request->input('activo_id'));
        $activo->novedad = $novedades;
        $activo->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function jsonRegistros(Request $request)
    {
        //$fecha = $this->input2Carbon($request->input('fechas'));
        $operador = Input::get('idop');
        $fdesde = Input::get('fdesde');
        $fhasta = Input::get('fhasta');
        //obtener luminaraia reflector
        $luminarias = $this->luminarias($fdesde, $fhasta, $operador);
        //Faroles terra
        $farolesTerra = $this->luminariasFarolTerra($fdesde, $fhasta, $operador);
        //obtener trafo
        $trafos = $this->trafos($fdesde, $fhasta, $operador);
        return response()->json([
            "faroles" => $farolesTerra,
            "trafos" => $trafos,
            "luminarias" => $luminarias
        ]);
    }


    private function luminarias($fdesde, $fhasta, $operador)
    {
        $ret = array();
        if ($operador == "x") {
            //$sql = "SELECT users.nombres,users.apellidos,activo.id,activo.latitud,activo.longitud,activo.fecha_registro,activo.hora,activo.direccion,activo.nomenclatura,iluminacion.id as eiid,iluminacion.serial,iluminacion.tipo,iluminacion.tecnologia,iluminacion.nombre,iluminacion.potencia,apoyo.id as apid,apoyo.pertenece as apPertence,apoyo.tipo as apTipo,apoyo.longitud as apLong,brazo.tipo as brzTipo,brazo.incorporado as incorporado,red.tipo as redTipo,red.aerea as redAerea,red.distancia as redDistancia,iluminacion.novedades as lumNovedades,apoyo.novedades as apNovedades,brazo.novedades as brzNovedades,red.novedades as redNovedades,iluminacion.foto,iluminacion.observacion as ilmObservacion, apoyo.observacion as apObservacion, brazo.observacion as brzObservacion, red.observacion as redObservacion FROM activo JOIN iluminacion ON iluminacion.activo_id = activo.id JOIN brazo ON brazo.iluminacion_id = iluminacion.id JOIN apoyo ON apoyo.activo_id = activo.id JOIN red ON red.activo_id = activo.id JOIN users ON users.id = activo.users_id WHERE (iluminacion.tipo = 1 OR iluminacion.tipo = 3) AND activo.fecha_registro BETWEEN ? AND ?";
            $sql2 = "SELECT users.nombres,users.apellidos,activo.id,activo.barrio,activo.latitud,activo.longitud,activo.fecha_registro,activo.hora,activo.direccion,activo.nomenclatura,iluminacion.id as eiid,iluminacion.serial,iluminacion.tipo,iluminacion.tecnologia,iluminacion.nombre,iluminacion.potencia,apoyo.id as apid,apoyo.pertenece as apPertence,apoyo.tipo as apTipo,apoyo.longitud as apLong,brazo.tipo as brzTipo,brazo.incorporado as incorporado,red.tipo as redTipo,red.aerea as redAerea,red.distancia as redDistancia,iluminacion.novedades as lumNovedades,apoyo.novedades as apNovedades,brazo.novedades as brzNovedades,red.novedades as redNovedades,iluminacion.foto,iluminacion.observacion as ilmObservacion,apoyo.observacion as apObservacion,brazo.observacion as brzObservacion,red.observacion as redObservacion  FROM activo, iluminacion, red, brazo, apoyo, users where activo.id = iluminacion.activo_id AND red.activo_id = activo.id AND brazo.iluminacion_id = iluminacion.id AND apoyo.activo_id = activo.id AND users.id = activo.users_id AND activo.fecha_registro BETWEEN ? AND ? AND (iluminacion.tipo = 1 OR iluminacion.tipo = 3) ORDER BY iluminacion.id ASC";
            $ret = DB::select($sql2, [$fdesde, $fhasta]);
        } else {
            //$sql = "SELECT users.nombres,users.apellidos,activo.id,activo.latitud,activo.longitud,activo.fecha_registro,activo.hora,activo.direccion,activo.nomenclatura,iluminacion.id as eiid,iluminacion.serial,iluminacion.tipo,iluminacion.tecnologia,iluminacion.nombre,iluminacion.potencia,apoyo.id as apid,apoyo.pertenece as apPertence,apoyo.tipo as apTipo,apoyo.longitud as apLong,brazo.tipo as brzTipo,brazo.incorporado as incorporado,red.tipo as redTipo,red.aerea as redAerea,red.distancia as redDistancia,iluminacion.novedades as lumNovedades,apoyo.novedades as apNovedades,brazo.novedades as brzNovedades,red.novedades as redNovedades,iluminacion.foto,iluminacion.observacion as ilmObservacion, apoyo.observacion as apObservacion, brazo.observacion as brzObservacion, red.observacion as redObservacion FROM activo JOIN iluminacion ON iluminacion.activo_id = activo.id JOIN brazo ON brazo.iluminacion_id = iluminacion.id JOIN apoyo ON apoyo.activo_id = activo.id JOIN red ON red.activo_id = activo.id JOIN users ON users.id = activo.users_id WHERE users.id = ? AND (iluminacion.tipo = 1 OR iluminacion.tipo = 3) AND activo.fecha_registro BETWEEN ? AND ?";
            $sql2 = "SELECT users.nombres,users.apellidos,activo.id,activo.barrio,activo.latitud,activo.longitud,activo.fecha_registro,activo.hora,activo.direccion,activo.nomenclatura,iluminacion.id as eiid,iluminacion.serial,iluminacion.tipo,iluminacion.tecnologia,iluminacion.nombre,iluminacion.potencia,apoyo.id as apid,apoyo.pertenece as apPertence,apoyo.tipo as apTipo,apoyo.longitud as apLong,brazo.tipo as brzTipo,brazo.incorporado as incorporado,red.tipo as redTipo,red.aerea as redAerea,red.distancia as redDistancia,iluminacion.novedades as lumNovedades,apoyo.novedades as apNovedades,brazo.novedades as brzNovedades,red.novedades as redNovedades,iluminacion.foto,iluminacion.observacion as ilmObservacion,apoyo.observacion as apObservacion,brazo.observacion as brzObservacion,red.observacion as redObservacion  FROM activo, iluminacion, red, brazo, apoyo, users where activo.id = iluminacion.activo_id AND red.activo_id = activo.id AND brazo.iluminacion_id = iluminacion.id AND apoyo.activo_id = activo.id AND users.id = activo.users_id AND users.id = ? AND activo.fecha_registro BETWEEN ? AND ? AND (iluminacion.tipo = 1 OR iluminacion.tipo = 3) ORDER BY iluminacion.id ASC";
            $ret = DB::select($sql2, [$operador, $fdesde, $fhasta]);
        }
        return $ret;
    }

    private function luminariasFarolTerra($fdesde, $fhasta, $operador)
    {

        $slq = "";
        if ($operador == "x") {
            //$slq = "SELECT users.nombres,users.apellidos,activo.id,activo.latitud,activo.longitud,activo.fecha_registro, activo.hora,activo.direccion,activo.nomenclatura,iluminacion.id as eiid,iluminacion.serial,iluminacion.tipo,iluminacion.tecnologia, iluminacion.nombre,iluminacion.potencia,red.tipo as redTipo,red.aerea as redAerea,red.distancia as redDistancia, iluminacion.novedades as lumNovedades,red.novedades as redNovedades,iluminacion.foto,iluminacion.observacion as ilumObservacion,red.observacion as redObservacion FROM  activo JOIN iluminacion ON iluminacion.activo_id = activo.id JOIN red ON red.activo_id = activo.id JOIN users ON users.id = activo.users_id WHERE (iluminacion.tipo = 2 OR iluminacion.tipo = 4) AND activo.fecha_registro BETWEEN ? AND ?";
            $sql2 = "SELECT users.nombres,users.apellidos,activo.id,activo.barrio,activo.latitud,activo.longitud,activo.fecha_registro, activo.hora,activo.direccion,activo.nomenclatura,iluminacion.id as eiid,iluminacion.serial,iluminacion.tipo,iluminacion.tecnologia, iluminacion.nombre,iluminacion.potencia,red.tipo as redTipo,red.aerea as redAerea,red.distancia as redDistancia, iluminacion.novedades as lumNovedades,red.novedades as redNovedades,iluminacion.foto,iluminacion.observacion as ilumObservacion,red.observacion as redObservacion FROM activo, iluminacion, red, users WHERE activo.id = iluminacion.activo_id AND red.activo_id = activo.id AND users.id = activo.users_id AND (iluminacion.tipo = 2 OR iluminacion.tipo = 4) AND activo.fecha_registro BETWEEN ? AND ?";
            $ret = DB::select($sql2, [$fdesde, $fhasta]);
        } else {
            //$slq = "SELECT users.nombres,users.apellidos,activo.id,activo.latitud,activo.longitud,activo.fecha_registro,activo.hora,activo.direccion,activo.nomenclatura,iluminacion.id as eiid,iluminacion.serial,iluminacion.tipo,iluminacion.tecnologia,iluminacion.nombre,iluminacion.potencia,red.tipo as redTipo,red.aerea as redAerea,red.distancia as redDistancia,iluminacion.novedades as lumNovedades,red.novedades as redNovedades,iluminacion.foto,iluminacion.observacion as ilumObservacion,red.observacion as redObservacion FROM  activo JOIN iluminacion ON iluminacion.activo_id = activo.id JOIN red ON red.activo_id = activo.id JOIN users ON users.id = activo.users_id WHERE users.id = ? AND (iluminacion.tipo = 2 OR iluminacion.tipo = 4) AND activo.fecha_registro BETWEEN ? AND ?";
            $sql2 = "SELECT users.nombres,users.apellidos,activo.id,activo.barrio,activo.latitud,activo.longitud,activo.fecha_registro, activo.hora,activo.direccion,activo.nomenclatura,iluminacion.id as eiid,iluminacion.serial,iluminacion.tipo,iluminacion.tecnologia, iluminacion.nombre,iluminacion.potencia,red.tipo as redTipo,red.aerea as redAerea,red.distancia as redDistancia, iluminacion.novedades as lumNovedades,red.novedades as redNovedades,iluminacion.foto,iluminacion.observacion as ilumObservacion,red.observacion as redObservacion FROM activo, iluminacion, red, users WHERE activo.id = iluminacion.activo_id AND red.activo_id = activo.id AND users.id = activo.users_id AND (iluminacion.tipo = 2 OR iluminacion.tipo = 4) AND users.id = ? AND activo.fecha_registro BETWEEN ? AND ?";
            $ret = DB::select($sql2, [$operador, $fdesde, $fhasta]);
        }
        return $ret;
    }

    private
    function trafos($fdesde, $fhasta, $operador)
    {
        $ret = array();

        $nfdesde = date("{$fdesde} 00:00:00");
        $nfhasta = date("{$fhasta} 23:59:59");

        if ($operador == "x") {
            $sql = "SELECT users.nombres,users.apellidos,trafo.id,trafo.latitud,trafo.longitud,trafo.serial,trafo.created_at,trafo.observacion,trafo.direccion, trafo.nomenclatura,apoyo.id as apid,apoyo.longitud as apLong,apoyo.pertenece as apPertenece,apoyo.tipo as apTipo,apoyo.observacion as apObservacion,trafo.potencia as potencia,apoyo.novedades as apNovedades,trafo.foto as foto FROM trafo JOIN apoyo ON apoyo.trafo_id = trafo.id JOIN users ON users.id = trafo.users_id WHERE trafo.created_at BETWEEN ? AND ?";
            $ret = DB::select($sql, [$nfdesde, $nfhasta]);
        } else {
            $sql = "SELECT users.nombres,users.apellidos,trafo.id,trafo.latitud,trafo.longitud,trafo.serial,trafo.created_at,trafo.observacion,trafo.direccion, trafo.nomenclatura,apoyo.id as apid,apoyo.longitud as apLong,apoyo.pertenece as apPertenece,apoyo.tipo as apTipo,apoyo.observacion as apObservacion,trafo.potencia as potencia,apoyo.novedades as apNovedades,trafo.foto as foto FROM trafo JOIN apoyo ON apoyo.trafo_id = trafo.id JOIN users ON users.id = trafo.users_id WHERE users.id = ? AND trafo.created_at BETWEEN ? AND ?";
            $ret = DB::select($sql, [$operador, $fdesde, $fhasta]);
        }

        return $ret;
    }

    private
    function input2Carbon($rango)
    {

        $fechas = array();
        //string(23) "08/04/2015 - 08/29/2015"
        $auxF = explode(" - ", $rango);
        $d = explode("/", $auxF[0]);
        $h = explode("/", $auxF[1]);

        $fechas[] = $desde = Carbon::createFromDate($d[2], $d[0], $d[1]);       // primero de enero del presente año
        $fechas[] = $hasta = Carbon::createFromDate($h[2], $h[0], $h[1]);     // 31 de diciembre del presente año

        return $fechas;
    }

    public function datosGenerales()
    {
        /*
         * Y, el ID relacionado a la georeferenciacion, asumo, que seria el de xada elemento luminico.
         * (Luminaria, farol, reflector, terrea, transfo)
         */
        $luminarias = DB::table('iluminacion')->where("tipo", "=", 1)->count();
        $faroles = DB::table('iluminacion')->where("tipo", "=", 2)->count();
        $reflectores = DB::table('iluminacion')->where("tipo", "=", 3)->count();
        $terras = DB::table('iluminacion')->where("tipo", "=", 4)->count();
        $trafos = DB::table('trafo')->count();
        $totalActual = $luminarias + $faroles + $reflectores + $terras;

        return response()->json([
            'luminarias' => $luminarias,
            'faroles' => $faroles,
            'reflectores' => $reflectores,
            'terras' => $terras,
            'trafos' => $trafos,
            'total' => $totalActual
        ]);
    }

    function excelRegistros()
    {
        $fHoy = Carbon::now('America/Bogota');
        $operador = Input::get('idop');
        $fdesde = Input::get("fdesde");
        $fhasta = Input::get("fhasta");


        header("Content-type: application/vnd.ms-excel; name='excel'; charset=utf-8");
        header("Content-Disposition: filename=inventariosalp_corte_" . $fdesde . "_" . $fhasta . ".xls");
        header("Pragma: no-cache");
        header("Expires: 0");
        echo utf8_decode($_POST['contenido']);


        /*
                Excel::create("Registros - {$fHoy}", function ($excel) use ($fdesde, $fhasta, $operador) {
                    $excel->sheet('Transformadores', function ($sheet) use ($fdesde, $fhasta, $operador) {
                        $trafos = $this->trafos($fdesde, $fhasta, $operador);
                        $sheet->loadView('admin.sistema.views.trafos', array(
                            'trafos' => $trafos,
                            'cont' => sizeof($trafos)
                        ));
                    });
                    $excel->sheet('Elementos de Iluminacion', function ($sheet) use ($fdesde, $fhasta, $operador) {

                        $luminarias = $this->luminarias($fdesde, $fhasta, $operador);
                        $farolesTerra = $this->luminariasFarolTerra($fdesde, $fhasta, $operador);
                        $sheet->loadView('admin.sistema.views.registros', array(
                            'luminarias' => $luminarias,
                            'farolest' => $farolesTerra,
                            'fdesde' => $fdesde,
                            'fhasta' => $fhasta
                        ));
                    });
                })->export('xlsx');

                */
    }

    public function nuevasDirecciones()
    {
        $user = Auth::user();
        return view("nuevasDirecciones", ['user' => $user]);
    }

    public function getActivos()
    {
        $activos = DB::table("activo")->limit(100)->get();
        return response()->json($activos);
    }

    public function updateAddress(Request $request)
    {
        /*$activo = Activo::find($request->id);
        $activo->direccion = $request->direccion;
        //$activo->barrio = $request->barrio;
        if($activo->save()){
            return response()->json(array("status" => "ok"));
        }
        return response()->json(array("status" => "fail"));*/
        $file = public_path() . '/file_restore_dir/CSV.csv';
        Excel::load($file, function ($reader) {
            // Loop through all sheets
            $rows = $reader->toArray();

            foreach ($rows as $row) {
                //dd($row['iddireccionbarrionomenclaturacomuna']);
                $col = explode(";", $row['iddireccionbarrionomenclaturacomuna']);
                try {//dd($col[0]);
                    $iluminacion = Iluminacion::find((int)$col[0]);
                    $activo = Activo::find($iluminacion->activo_id);
                    if ($activo) {
                        $activo->direccion = $col[1];
                        $activo->barrio = $col[2];
                        $activo->nomenclatura = $col[3];
                        $activo->comuna = $col[4];
                        $activo->save();
                        //dd($activo->id);
                    }
                } catch (Exception $e) {
                    dd($col[0]);
                }
            }
        });
    }

    public function borrarActivos()
    {
        $file = public_path() . '/file_delete_dir/CSV.csv';
        Excel::load($file, function ($reader) {
            // Loop through all sheets
            $rows = $reader->toArray();
            $resp = array();

            foreach ($rows as $row) {
                //dd((int)$row['id']);
                //$col = explode(";", $row['id']);
                try {//dd($col[0]);
                    $iluminacion = Iluminacion::find((int)$row['id']);
                    if ($iluminacion) {
                        $activo = Activo::find($iluminacion->activo_id);
                        if ($activo) {
                            //dd($activo->id);
                            $b_red = $this->deleteElemento($activo->id, 'red');
                            if ($b_red) {
                                $b_apoyo = $this->deleteElemento($activo->id, 'apoyo');
                                if ($b_apoyo) {
                                    $iluminacion = Iluminacion::where('activo_id', '=', $activo->id)->get();
                                    foreach ($iluminacion as $il) {
                                        $brazos = Brazo::where('iluminacion_id', '=', $il->id)->get();
                                        $i = count($brazos);
                                        foreach ($brazos as $brz) {
                                            $b_brazo = $this->deleteElemento($brz->id, 'brazo');
                                            $i--;
                                        }
                                        if ($i == 0) {
                                            $b_iluminacion = $this->deleteElemento($il->id, 'iluminacion');
                                            if ($b_iluminacion) {
                                                //return response()->json(array("status" => "fail", "error" => "not_delete_red"));
                                                array_push($resp, (int)$row['id']);
                                            }
                                        }
                                    }

                                }
                            }
                            //return response()->json(array("status" => "fail", "error" => "not_delete_red"));
                        }
                    }
                } catch (Exception $e) {
                    dd((int)$row['id']);
                }
            }
            return response()->json(array("status" => "ok", "msj" => "delete_all_items_selected"));
        });
        return response()->json(array("status" => "fail", "error" => "not_id"));
    }

    private function deleteElemento($id, $tipo)
    {
        if ($id) {
            if ($tipo === 'red') {
                $red = Red::where('activo_id', '=', $id)->first();
                if ($red) {
                    if ($red->delete()) {
                        return true;
                    }
                }
            } else if ($tipo === 'apoyo') {
                $apoyo = Apoyo::where('activo_id', '=', $id)->first();
                if ($apoyo) {
                    if ($apoyo->delete()) {
                        return true;
                    }
                }
            } else if ($tipo === 'brazo') {
                $brazo = Brazo::find($id);
                if ($brazo) {
                    if ($brazo->delete()) {
                        return true;
                    }
                }
            } else if ($tipo === 'iluminacion') {
                $iluminacion = Iluminacion::find($id);
                if ($iluminacion) {
                    if ($iluminacion->delete()) {
                        return true;
                    }
                }
            }
        }
        return false;
    }


    function editActivos()
    {
        $file = public_path() . '/file_edit_dir/CSV.csv';
        //$file = 'public/file_edit_dir/CSV.csv';
        Excel::load($file, function ($reader) {
            // Loop through all sheets
            $rows = $reader->toArray();
            $i = 1;
            foreach ($rows as $row) {
                $col = explode(";", $row['id']);
                //dd($col);
                try {//dd($col[0]);
                    $iluminacion = Iluminacion::find((int)($col[0]));
                    if ($iluminacion) {
                        $activo = Activo::find($iluminacion->activo_id);
                        if ($activo) {
                            //pos 2 to 11, info activo
                            //pos 12 to 17, info iluminacion
                            //pos 18 to 20, info farol
                            //pos 21 to 24, info reflector
                            //pos 25 to 26, info terra
                            //pos 27 obs iluminacion
                            //pos 28 to 32, apoyo cens
                            //pos 33 apoyo telecom
                            //pos 34 to 36 apoyo exlu
                            //pos 37 obs apoyo
                            //pos 38 to 39, info brazo
                            //pos 40 obs brazo
                            //pos 41 red cens
                            //pos 42 to 43, red excl
                            //pos 44 obs red
                            //pos 45 to 52 nov luminaria
                            //pos 53 to 58 nov apoyo
                            //pos 59 to 61 nov brazo
                            //pos 62 to 64 nov red
                            $this->updateActivo($activo, $col[5], $col[6], $col[8], $col[9], $col[3], $col[2], $col[4], $col[10]);
                            switch ($iluminacion->tipo) {
                                case 1:
                                    $tecnologia = $this->validaTecnologiaIl($col[12], $col[13], $col[14], $col[15], $col[16], $col[17]);
                                    if ($tecnologia)
                                        $this->updateIluminacion($iluminacion, $tecnologia['tipo'], $tecnologia['nombre'], $tecnologia['potencia'], $col[27], $col[11], $col[38], $col[39], $col[40]);
                                    break;
                                case 2:
                                    $tecnologia = $this->validaTecnologiaFrl($col[18], $col[19], $col[20]);
                                    if ($tecnologia)
                                        $this->updateFarol($iluminacion, $tecnologia['tipo'], $tecnologia['nombre'], $tecnologia['potencia'], $col[27], $col[11]);
                                    break;
                                case 3:
                                    $tecnologia = $this->validaTecnologiaRfl($col[21], $col[22], $col[23], $col[24]);
                                    if ($tecnologia)
                                        $this->updateReflector($iluminacion, $tecnologia['tipo'], $tecnologia['nombre'], $tecnologia['potencia'], $col[27], $col[11]);
                                    break;
                                case 4:
                                    $tecnologia = $this->validaTecnologiaTrr($col[25], $col[26]);
                                    if ($tecnologia)
                                        $this->updateTerra($iluminacion, $tecnologia['tipo'], $tecnologia['nombre'], $tecnologia['potencia'], $col[27], $col[11]);
                                    break;
                            }

                            $tecAp = $this->validaTecnologiaAp($col[28], $col[29], $col[30], $col[31], $col[32], $col[33], $col[34], $col[35], $col[36]);
                            if ($tecAp)
                                $this->updateApoyo($activo->id, $tecAp['pertenece'], $tecAp['tipo'], $tecAp['longitud'], $col[37]);

                            $tecRed = $this->validaTecnologiaRed($col[41], $col[42], $col[43]);
                            if($tecRed)
                                $this->updateRed($activo->id, $tecRed['tipo'], $tecRed['distancia'], $tecRed['aerea'], $col[44]);
                        }
                        $i++;
                    }else{
                        return response()->json(array("status" => "fail", "error" => "not_id - {$i} _ id_ {$col[0]}"));
                    }
                } catch (Exception $e) {
                    dd((int)$row['id']);
                }
            }
            return response()->json(array("status" => "ok", "msj" => "{$i} - updated_all_items_selected"));
        });
        return response()->json(array("status" => "ok", "result" => "updated_all_items_selected"));
    }

    private function updateActivo($activo, $latitud, $longitud, $fecha_registro, $hora, $direccion, $nomenclatura, $barrio, $comuna)
    {
        if ($activo) {
            if ($latitud)
                $activo->latitud = $latitud;
            if ($longitud)
                $activo->longitud = $longitud;
            //if ($fecha_registro)
            //    $activo->fecha_registro = $fecha_registro;
            if ($hora)
                $activo->hora = $hora;
            if ($direccion)
                $activo->direccion = $direccion;
            if ($nomenclatura)
                $activo->nomenclatura = $nomenclatura;
            if ($barrio)
                $activo->barrio = $barrio;
            if ($comuna)
                $activo->comuna = $comuna;

            $activo->save();
            return true;
        }
        return false;
    }

    private function updateIluminacion($iluminacion, $tecnologia, $nombre, $potencia, $obs, $serial, $b_tipo1, $b_tipo2, $b_obs)
    {
        if ($iluminacion) {
            if ($tecnologia)
                $iluminacion->tecnologia = $tecnologia;
            if ($nombre)
                $iluminacion->nombre = $nombre;
            if ($potencia)
                $iluminacion->potencia = $potencia;
            if ($obs)
                $iluminacion->observacion = $obs;
            if ($serial)
                $iluminacion->serial = $serial;
            $iluminacion->save();
        }
        $brazo = Brazo::where('iluminacion_id', '=', $iluminacion->id)->first();
        if ($brazo) {
            if ($b_tipo1 == 'Incorporado') {
                $brazo->tipo = 1;
                $brazo->incorporado = 1;
            } else if ($b_tipo1 == 'No Incorporado') {
                $brazo->tipo = 1;
                $brazo->incorporado = 0;
            }
            if ($b_tipo2 == 'Incorporado') {
                $brazo->tipo = 2;
                $brazo->incorporado = 1;
            } else if ($b_tipo2 == 'No Incorporado') {
                $brazo->tipo = 2;
                $brazo->incorporado = 0;
            }
            if ($obs)
                $brazo->observacion = $b_obs;

            $brazo->save();
        }
        return true;
    }

    private function updateReflector($reflector, $tecnologia, $nombre, $potencia, $obs, $serial)
    {
        if ($reflector) {
            if ($tecnologia)
                $reflector->tecnologia = $tecnologia;
            if ($nombre)
                $reflector->nombre = $nombre;
            if ($potencia)
                $reflector->potencia = $potencia;
            if ($obs)
                $reflector->observacion = $obs;
            if ($serial)
                $reflector->serial = $serial;
            $reflector->save();
            return true;
        }
        return false;
    }

    private function updateFarol($farol, $tecnologia, $nombre, $potencia, $obs, $serial)
    {
        if ($farol) {
            if ($tecnologia)
                $farol->tecnologia = $tecnologia;
            if ($nombre)
                $farol->nombre = $nombre;
            if ($potencia)
                $farol->potencia = $potencia;
            if ($obs)
                $farol->observacion = $obs;
            if ($serial)
                $farol->serial = $serial;

            $farol->save();
            return true;
        }
        return null;
    }

    private function updateTerra($terra, $tecnologia, $nombre, $potencia, $obs, $serial)
    {
        if ($terra) {
            if ($tecnologia)
                $terra->tecnologia = $tecnologia;
            if ($nombre)
                $terra->nombre = $nombre;
            if ($potencia)
                $terra->potencia = $potencia;
            if ($obs)
                $terra->observacion = $obs;
            if ($serial)
                $terra->serial = $serial;

            $terra->save();
            return true;
        }
        return null;
    }

    private function updateApoyo($activo_id, $pertenece, $tipo, $longitud, $observacion)
    {
        if ($activo_id) {
            $apoyo = Apoyo::where('activo_id', '=', $activo_id)->first();
            if ($apoyo) {
                if ($pertenece)
                    $apoyo->pertenece = $pertenece;
                if ($tipo)
                    $apoyo->tipo = $tipo;
                if ($longitud)
                    $apoyo->longitud = $longitud;
                if ($observacion)
                    $apoyo->observacion = $observacion;

                $apoyo->save();
                return true;
            }
        }
        return null;
    }

    private function updateRed($activo_id, $tipo, $distancia, $aerea, $obs)
    {
        if($activo_id){
            $red = Red::where('activo_id', '=', $activo_id)->first();
            if($red){
                if($tipo)
                    $red->tipo = $tipo;
                if($distancia)
                    $red->distancia = $distancia;
                if($aerea)
                    $red->aerea = $aerea;
                if($obs)
                    $red->observacion = $obs;

                $red->save();
                return true;
            }
        }
        return null;
    }

    private function validaTecnologiaIl($sd, $t_ld, $ld, $mh, $mc, $lmx)
    {
        if ($sd) {
            return ['tipo' => 'sodio', 'nombre' => $t_ld, 'potencia' => $sd];
        } else if ($ld) {
            return ['tipo' => 'led', 'nombre' => $t_ld, 'potencia' => $ld];
        } else if ($mh) {
            return ['tipo' => 'metal halide', 'nombre' => $t_ld, 'potencia' => $mh];
        } else if ($mc) {
            return ['tipo' => 'mercurio', 'nombre' => $t_ld, 'potencia' => $mc];
        } else if ($lmx) {
            return ['tipo' => 'luz mixta', 'nombre' => $t_ld, 'potencia' => $lmx];
        }
        return null;
    }

    private function validaTecnologiaRfl($sd, $ld, $mh, $mc)
    {
        if ($sd) {
            return ['tipo' => 'sodio', 'nombre' => '', 'potencia' => $sd];
        } else if ($ld) {
            return ['tipo' => 'led', 'nombre' => '', 'potencia' => $ld];
        } else if ($mh) {
            return ['tipo' => 'metal halide', 'nombre' => '', 'potencia' => $mh];
        } else if ($mc) {
            return ['tipo' => 'mercurio', 'nombre' => '', 'potencia' => $mc];
        }

        return null;
    }

    private function validaTecnologiaFrl($sd, $mh, $ld)
    {
        if ($sd) {
            return ['tipo' => 'sodio', 'nombre' => '', 'potencia' => $sd];
        } else if ($ld) {
            return ['tipo' => 'led', 'nombre' => '', 'potencia' => $ld];
        } else if ($mh) {
            return ['tipo' => 'metal halide', 'nombre' => '', 'potencia' => $mh];
        }
        return null;
    }

    private function validaTecnologiaTrr($sd, $ld)
    {
        if ($sd) {
            return ['tipo' => 'sodio', 'nombre' => '', 'potencia' => $sd];
        } else if ($ld) {
            return ['tipo' => 'led', 'nombre' => '', 'potencia' => $ld];
        }
        return null;
    }

    private function validaTecnologiaAp($conc_cns, $mt_cns, $fb_cns, $rl_cns, $mdr_cns, $conc_tc, $conc_ex, $mt_ex, $fb_ex)
    {
        if ($conc_cns)
            return ['pertenece' => 1, 'tipo' => 1, 'longitud' => $conc_cns];
        else if ($mt_cns)
            return ['pertenece' => 1, 'tipo' => 2, 'longitud' => $mt_cns];
        else if ($fb_cns)
            return ['pertenece' => 1, 'tipo' => 3, 'longitud' => $fb_cns];
        else if ($rl_cns)
            return ['pertenece' => 1, 'tipo' => 4, 'longitud' => $rl_cns];
        else if ($mdr_cns)
            return ['pertenece' => 1, 'tipo' => 5, 'longitud' => $mdr_cns];
        else if ($conc_tc)
            return ['pertenece' => 2, 'tipo' => 1, 'longitud' => $conc_tc];
        else if ($conc_ex)
            return ['pertenece' => 3, 'tipo' => 1, 'longitud' => $conc_ex];
        else if ($mt_ex)
            return ['pertenece' => 3, 'tipo' => 2, 'longitud' => $mt_ex];
        else if ($fb_ex)
            return ['pertenece' => 3, 'tipo' => 3, 'longitud' => $fb_ex];

        return null;
    }

    private function validaTecnologiaRed($cens, $aer_sub, $dist)
    {
        if($cens){
            return ['tipo' => 1, 'distancia' => '', 'aerea' => ''];
        }else{
            if($aer_sub == 'Aerea'){
                return ['tipo' => 2, 'distancia' => $dist, 'aerea' => 1];
            }else if($aer_sub == 'Subterranea'){
                return ['tipo' => 2, 'distancia' => $dist, 'aerea' => 2];
            }
        }
        return null;
    }

}
