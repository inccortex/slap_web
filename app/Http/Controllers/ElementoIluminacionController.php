<?php

namespace App\Http\Controllers;

use App\Iluminacion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;


class ElementoIluminacionController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        $content = base64_decode($request->input('foto'));
        $nuevo = Iluminacion::create($request->all());
               
        Storage::put("activos/". $nuevo->apoyo->activo->id . "/" . $nuevo->id . ".jpg", $content);
        $nuevo->foto = 'activos/' . $nuevo->apoyo->activo->id . "/" . $nuevo->id . ".jpg";
        $nuevo->save();

        if ($nuevo) {
            return response()->json(['state' => 'ok', 'message' => 'Se registró el activo', 'id' => encrypt($nuevo->id)]);
        }
        return response()->json(['estate' => 'error', 'message' => 'No se registró el activo', 'id' => -1]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

}
