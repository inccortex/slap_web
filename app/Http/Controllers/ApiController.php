<?php

namespace App\Http\Controllers;

use App\Red;
use App\User;
use App\Apoyo;
use App\Brazo;
use App\Trafo;
use App\Activo;
use App\Iluminacion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;

class ApiController extends Controller
{

    /**
     * @function - verifica si el usuario administrador esta logueado, y muestra el home del usuario
     * de lo contrario lo devuelve a la página de logueo
     * @return View
     */

    function getIndex()
    {
        $user = Auth::user();
        if ($user) {
            if ($user->type == "Administrador") {
                return view("admin.index", ['user' => $user]);
            } else if ($user->type == "Revisor") {
                return view("revisor.index", ['user' => $user]);
            }
        }
        Auth::logout();
        return view("welcome")->with("error", "No tiene permisos para acceder a esta acción");
    }


    /**
     * @function - permite subir una foto al servidor
     * @param $archivo - archivo a subir
     * @param $id - identificador del usuario al que se asigna la foto subida
     * @return string - ruta del servidor donde se almacena la imagen
     */
    function subir_fotos($archivo, $id)
    {
        $nombre = $archivo->getClientOriginalName();
        $extension = explode('.', $nombre);
        $ruta_disco = public_path() . '/img/fotosUsuario';
        $archivo->move($ruta_disco, "{$id}.{$extension[1]}");
        $ruta = "/img/fotosUsuario/{$id}.{$extension[1]}";
        return $ruta;
    }

    function cerrarOperaciones()
    {
        $user = Auth::user();
        if ($user) {
            DB::table('users')
                ->update(['users.puede_registrar' => 0, "users.meta_hoy" => 0]);
        }
        return redirect("/")->with("error", "No tiene permisos para realizar esta acción.");
    }

    function abrirOperaciones()
    {
        $user = Auth::user();
        if ($user) {
            DB::table('users')
                ->update(['users.puede_registrar' => 1, "users.meta_hoy" => 0]);
        }
        return redirect("/")->with("error", "No tiene permisos para realizar esta acción.");
    }

    public function guardarLuminariaReflector(Request $request)
    {


        try {
            DB::beginTransaction();         // inicia la transacción
            $activo = Activo::create(["users_id" => $request->input('users_id')]);
            $activo->fill($request->input("activo"))->save();

            $apoyo = Apoyo::create(["activo_id" => $activo->id]);
            $apoyo->fill($request->input("apoyo"))->save();

            $red = Red::create(["activo_id" => $activo->id]);
            $red->fill($request->input("red"))->save();

            $eis = $request->input("EI");
            foreach ($eis as $ei) {

                // crear iluminación
                $nuevo = Iluminacion::create(["activo_id" => $activo->id]);
                $nuevo->fill($ei[0]);
                $nuevo->activo_id = $activo->id;
                $content = base64_decode($ei[0]["foto"]);
                Storage::disk('public')->put("activos/" . $nuevo->activo_id . "/" . $nuevo->id . ".jpg", $content);


                $nuevo->foto = 'activos/' . $nuevo->activo_id . "/" . $nuevo->id . ".jpg";
                $nuevo->save();

                // crear brazo
                $brazo = Brazo::create(["iluminacion_id" => $nuevo->id]);
                $brazo->fill($ei[1])->save();
            }
            $this->llevarConteoMeta($request->input("users_id"));

            DB::commit();          // confirma la transaccción
            return json_encode(["status" => "ok", "msj" => "Activo registrado con exito"]);
        } Catch (ModelNotFoundException $e) {
            DB::rollback();         // revierte las modificaciones en la base de datos
            return json_encode(["status" => "fail", "msj" => "No se pudo guardar el activo"]);
        }
    }

    public function guardarFarol(Request $request)
    {

        try {
            DB::beginTransaction();         // inicia la transacción

            $activo = Activo::create(["users_id" => $request->input('users_id')]);
            $activo->fill($request->input("activo"))->save();

            Apoyo::create(["activo_id" => $activo->id, "observacion" => "Tipo farol"]);

            $aux = $request->input("EI");
            $i = Iluminacion::create(["activo_id" => $activo->id]);
            $content = base64_decode($aux["foto"]);
            Storage::disk('public')->put("activos/" . $i->activo_id . "/" . $i->id . ".jpg", $content);
            $i->foto = 'activos/' . $i->activo_id . "/" . $i->id . ".jpg";
            $i->fill($request->input("EI"))->save();

            $red = Red::create($request->input("red"));
            $red->activo_id = $activo->id;
            $red->save();

            Brazo::create(["iluminacion_id" => $i->id, "observacion" => "Brazo tipo farol", "iluminacion_id" => $i->id]);
            $this->llevarConteoMeta($request->input("users_id"));

            DB::commit();          // confirma la transaccción
            return json_encode(["status" => "ok", "msj" => "Activo registrado con exito"]);
        } Catch (ModelNotFoundException $e) {
            DB::rollback();         // revierte las modificaciones en la base de datos
            return json_encode(["status" => "fail", "msj" => "No se pudo guardar el activo"]);
        }
    }

    public function guardarTerreo(Request $request)
    {

        try {
            DB::beginTransaction();         // inicia la transacción

            $activo = Activo::create(["users_id" => $request->input('users_id')]);
            $activo->fill($request->input("activo"))->save();
            $aux = $request->input("EI");

            $i = Iluminacion::create(["activo_id" => $activo->id]);
            $content = base64_decode($aux["foto"]);
            Storage::disk('public')->put("activos/terras/" . $i->id . ".jpg", $content);
            $i->foto = 'activos/terras/' . $i->id . ".jpg";
            $i->fill($request->input("EI"))->save();

            $red = Red::create($request->input("red"));
            $red->activo_id = $activo->id;
            $red->save();

            $this->llevarConteoMeta($request->input("users_id"));

            DB::commit();          // confirma la transaccción
            return json_encode(["status" => "ok", "msj" => "Activo registrado con exito"]);
        } Catch (ModelNotFoundException $e) {
            DB::rollback();         // revierte las modificaciones en la base de datos
            return json_encode(["status" => "fail", "msj" => "No se pudo guardar el activo"]);
        }
    }

    public function guardarTrafo(Request $request)
    {

        try {
            DB::beginTransaction();         // inicia la transacción

            $trafo = Trafo::create([$request->input("users_id")]);
            $trafo->fill($request->input("trafo"));
            $aux = $request->input("trafo");
            $content = base64_decode($aux["foto"]);
            Storage::disk('public')->put("trafo/" . $trafo->id . ".jpg", $content);
            $trafo->foto = 'trafo/' . $trafo->id . ".jpg";
            $trafo->save();

            $apoyos = $request->input("apoyo");
            foreach ($apoyos as $a) {
                $i = Apoyo::create($a);
                $i->trafo_id = $trafo->id;
                $i->save();
            }
            $this->llevarConteoMeta($request->input("users_id"));

            DB::commit();          // confirma la transaccción
            return json_encode(["status" => "ok", "msj" => "Trafo registrado con exito"]);
        } Catch (ModelNotFoundException $e) {
            DB::rollback();         // revierte las modificaciones en la base de datos
            return json_encode(["status" => "fail", "msj" => "No se pudo guardar el trafo"]);
        }
    }

    public function llevarConteoMeta($userOp)
    {

        $fHoy = Carbon::now('America/Bogota')->format('Y-m-d');
        // busco la meta de la fecha
        $meta_hoy = DB::table('meta_operador')
            ->where("users_id", "=", $userOp)
            ->whereRaw("meta_operador.fecha = ?", array($fHoy))
            ->first();

        //actualizo la meta de hoy
        if ($meta_hoy) {
            DB::table('meta_operador')
                ->where('meta_operador.users_id', "=", $userOp)
                ->whereRaw("meta_operador.fecha = ?", array($fHoy))
                ->update(['meta_operador.realizado' => $meta_hoy->realizado + 1]);
        }
    }

    public function getPendientes(Request $request)
    {


        $user = User::where('id', $request->input('users_id'))->where('type', "Supervisor")->first();
        $pendientes = [];

        // apoyos
        $user->activosDeSuper()->with('Apoyo')->each(function ($value) use (&$pendientes) {
            if ($value->v_pertenece) {
                $pendientes[] = $value;
            } else if ($value->v_tipo) {
                $pendientes[] = $value;
            } else if ($value->v_longitud) {
                $pendientes[] = $value;
            }
        });

        // iluminacion
        $user->activosDeSuper()->with('Iluminacion')->each(function ($value) use (&$pendientes) {
            if ($value->v_tecnología) {
                $pendientes[] = $value;
            } else if ($value->v_potencia) {
                $pendientes[] = $value;
            }
        });

        // redes
        $user->activosDeSuper->with('Red')->each(function ($value) use (&$pendientes) {
            if ($value->v_tipo) {
                $pendientes[] = $value;
            } else if ($value->v_distancia) {
                $pendientes[] = $value;
            }
        });

        // brazos
        $user->activosDeSuper()->with('Iluminacion.Brazo')->each(function ($value) use (&$pendientes) {
            if ($value->v_tipo) {
                $pendientes[] = $value;
            }
        });

        return $pendientes;
    }

    public function indicadoresOperador(Request $request)
    {

        $indicadores = [];
        $fHoy = Carbon::now('America/Bogota')->format('Y-m-d');
        $operador = User::where('id', $request->input('users_id'))->where('type', "Operador")->first();

        if ($operador) {

            /*$indicadores["meta_hoy"] = $operador->meta_hoy;
           $aux = $operador->metaOperador()->where('fecha', $fHoy)->first();
           is_null($aux) ? $resultado = 0 : $resultado = $aux->realizado;
           $indicadores["realizado"] = $resultado;
           $indicadores["meta_global"] = $operador->meta_global;
           $indicadores["historico"] = $operador->activos()->count(); + $operador->trafos()->count();*/

            $histEi = DB::table('activo')
                ->join('iluminacion', 'iluminacion.activo_id', '=', 'activo.id')
                ->where('activo.users_id', '=', $operador->id)
                ->count();
            $hstTrafo = DB::table('trafo')
                ->where('trafo.users_id', '=', $operador->id)
                ->count();
            $metaGlobal = DB::table('meta_operador')
                ->where("users_id", '=', $operador->id)
                ->sum('meta');


            $sql = "SELECT COUNT(activo.id) as cont FROM activo JOIN iluminacion ON iluminacion.activo_id = activo.id WHERE activo.fecha_registro BETWEEN ? AND ? AND activo.users_id = ?";
            $realizado = DB::select($sql, [$fHoy, $fHoy, $operador->id]);

            $indicadores["meta_hoy"] = $operador->meta_hoy;
            $indicadores["realizado"] = $realizado[0]->cont;
            $indicadores["meta_global"] = $metaGlobal;
            $indicadores["historico"] = $histEi + $hstTrafo;

            return json_encode(["status" => "ok", "msj" => $indicadores]);
        }
        return response()->json('error', 404);
    }

    public function indicadoresSupervisor(Request $request)
    {


        $indicadores = [];
        $fHoy = Carbon::now('America/Bogota')->format('Y-m-d');

        $supervisor = User::where('id', $request->input('users_id'))->where('type', "Supervisor")->first();

        if ($supervisor) {
            $indicadores["meta_hoy"] = $supervisor->operarios()->sum('meta_hoy');
            $aux = $supervisor->metaSupervisor()->where('fecha', $fHoy)->first();
            is_null($aux) ? $resultado = 0 : $resultado = $aux->realizado;
            $indicadores["realizado"] = $resultado;
            $indicadores["meta_global"] = $supervisor->meta_global;
            $indicadores["historico"] = $supervisor->operarios()->with('activo')->count() + $supervisor->operarios()->with('trafo')->count();

            return json_encode(["status" => "ok", "msj" => $indicadores]);
        }
        return response()->json('error_user', 404);
    }

    public function operadoresSupervisorReg(Request $request)
    {

        $fHoy = Carbon::now('America/Bogota')->format('Y-m-d');
        $supervisor = User::where('id', $request->input('users_id'))->where('type', "Supervisor")->first();

        if ($supervisor) {
            $sql = "SELECT users.id,CONCAT(users.nombres, ' ', users.apellidos) as nombres, users.meta_hoy, users.meta_global, users.profile_image, (SELECT COUNT(activo.id) FROM activo WHERE activo.users_id = users.id) as registros, (SELECT COUNT(activo.id) FROM activo WHERE activo.users_id = users.id AND activo.fecha_registro BETWEEN ? AND ?) as registrosHoy FROM users WHERE users.id IN (SELECT users.id FROM users WHERE users.supervisor_id = ?)";
            $operadores = DB::select($sql, [$fHoy, $fHoy, $supervisor->id]);
            return json_encode(["status" => "ok", "msj" => $operadores]);
        }
        return response()->json('error_user', 404);

    }

    public function saveValidacionSuperEI(Request $request)
    {
        //dd($request->ei);
        $iluminacion = Iluminacion::find($request->ei['ilID']);
        if ($request->ei['ilTecnologia']) {
            $iluminacion->tecnologia = $request->ei['ilTecnologia'];
        }
        if ($request->ei['ilNombre']) {
            $iluminacion->nombre = $request->ei['ilNombre'];
        }
        if ($request->ei['ilPotencia']) {
            $iluminacion->potencia = $request->ei['ilPotencia'];
        }
        $iluminacion->v_tecnologia = 0;
        $iluminacion->v_potencia = 0;
        $iluminacion->save();

        $brazo = Brazo::find($request->ei['idBrz']);
        if ($request->ei['brzTipo']) {
            $brazo->tipo = $request->ei['brzTipo'];
        }
        if ($request->ei['brzIncorp']) {
            $brazo->incorporado = $request->ei['brzIncorp'];
        }
        $brazo->v_tipo = 0;
        $brazo->save();

        $apoyo = Apoyo::find($request->ei['apId']);
        if ($request->ei['apPertenece']) {
            $apoyo->pertenece = $request->ei['apPertenece'];
        }
        if ($request->ei['apTipo']) {
            $apoyo->tipo = $request->ei['apTipo'];
        }
        if ($request->ei['apLongitud']) {
            $apoyo->longitud = $request->ei['apLongitud'];
        }
        $apoyo->v_pertenece = 0;
        $apoyo->v_tipo = 0;
        $apoyo->v_longitud = 0;
        $apoyo->save();

        $red = Red::find($request->ei['redId']);
        if ($request->ei['redTipo']) {
            $red->tipo = $request->ei['redTipo'];
        }
        if ($request->ei['redDistancia']) {
            $red->distancia = $request->ei['redDistancia'];
        }
        if ($request->ei['redArerea']) {
            $red->aerea = $request->ei['redArerea'];
        }
        $red->v_tipo = 0;
        $red->v_distancia = 0;
        $red->save();
        return response()->json(["status" => "ok", "msj" => "Validado"]);
    }

    public function saveValidacionSuperFT(Request $request)
    {
        //dd($request->ft);
        $iluminacion = Iluminacion::find($request->ft['ilID']);
        if($request->ft['ilTipo']){
            $iluminacion->tipo = $request->ft['ilTipo'];
        }
        if($request->ft['ilTecnologia']){
            $iluminacion->tecnologia = $request->ft['ilTecnologia'];
        }
        if($request->ft['ilNombre']){
            $iluminacion->nombre = $request->ft['ilNombre'];
        }
        if($request->ft['ilPotencia']){
            $iluminacion->potencia = $request->ft['ilPotencia'];
        }
        $iluminacion->v_tecnologia = 0;
        $iluminacion->v_potencia = 0;
        $iluminacion->save();

        $red = Red::find($request->ft['redId']);
        if($request->ft['redTipo']){
            $red->tipo = $request->ft['redTipo'];
        }
        if($request->ft['redDistancia']){
            $red->distancia = $request->ft['redDistancia'];
        }
        if($request->ft['redArerea']){
            $red->aerea = $request->ft['redArerea'];
        }
        $red->v_tipo = 0;
        $red->v_distancia = 0;
        $red->save();
        return response()->json(["status" => "ok", "msj" => "Validado"]);
    }

    public function saveValidacionSuperTRF(Request $request)
    {
        dd($request->trf);
        $trafo = Trafo::find($request->trf['idTrafo']);
        if($request->trf['trafoPotencia']){
            $trafo->potencia = $request->trf['trafoPotencia'];
        }
        $trafo->trafo_validar = 0;
        $trafo->save();

        $apoyo = Apoyo::find($request->trf['idTrafo']);

    }

    public function datosGeneralesOp (){
        $fHoy = Carbon::now('America/Bogota')->format('Y-m-d');
        $sqlHistoricos = "SELECT COUNT(activo.id) as cant, CONCAT(users.nombres, ' ', users.apellidos) as op FROM activo JOIN users ON activo.users_id = users.id WHERE users.state = 1 GROUP BY op";
        $historicos = DB::select($sqlHistoricos);
        $sqlRegistrosHoy = "SELECT COUNT(activo.id) as cant, CONCAT(users.nombres, ' ', users.apellidos) as op FROM activo JOIN users ON activo.users_id = users.id WHERE users.state = 1 AND activo.fecha_registro BETWEEN '2017-06-27' AND '2017-06-27' GROUP BY op";
        $registrosHoy = DB::select($sqlRegistrosHoy, [$fHoy, $fHoy]);
        $sqlHistoricoBarrios = "SELECT COUNT(activo.id) as cant, activo.barrio as op FROM activo JOIN users ON activo.users_id = users.id WHERE users.state = 1 GROUP BY activo.barrio";
        $historicoBarrios = DB::select($sqlHistoricoBarrios);
        return response()->json([
            'historicos' => $historicos,
            'registrosHoy' => $registrosHoy,
            'historicosBarrios' => $historicoBarrios
        ]);
    }

}
