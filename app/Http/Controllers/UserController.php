<?php

namespace App\Http\Controllers;

use App\User;
use Carbon\Carbon;
use App\MetaOperador;
use App\MetaSupervisor;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Validation\Rules\In;
use Symfony\Component\HttpFoundation\IpUtils;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use JWTAuth;
use JWTAuthException;

class UserController extends Controller {

    /**
     * @function - verifica los datos de acceso ingresados por el usuario para iniciar sesión, y realiza el login
     * @return - View home si inicia sesion || welcome si no inicia sesion
     */
    function loginWeb()
    {
        $user = Input::get("user");
        $psw = Input::get("password");

        if ($user && $psw) {
            $usuario = User::where("user", "LIKE", $user)->first();
            if (!Hash::check($psw, $usuario->password)) {
                Auth::logout();
                return redirect("/")->with("error", "El nombre de usuario y la contraseña no coincide, verifique e intentelo de nuevo.");
            }
            if ($usuario && Hash::check($psw, $usuario->password)) {
                Auth::login($usuario);
                return redirect("/")->with("msj", "Se ha iniciado sesión exitosamente.");
            } else {
                Auth::logout();
                return redirect("/")->with("error", "El nombre de usuario y la contraseña no coincide, verifique e intentelo de nuevo.");
            }
        }
        return redirect("/")->with("error", "No se reconocen los datos que de ingreso, por favor verifique e intentelo de nuevo.");
    }

    /**
     * @function - cierra sesión en la web de administracion
     * @return \Illuminate\Http\RedirectResponse
     */
    function logoutWeb() {
        if (Auth::check()) {
            Auth::logout();
            return redirect("/")->with("msj", "Se ha cerrado la sesión.");
        }
        return redirect("/")->with("error", "Se ha intentado cerrar la sesión, pero no hay sesiones abiertas.");
    }

    /**
     * @function - listado de usuarios registrados en el sisitema, habilitados inhabilitados
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    function users() {
        $user = Auth::user();
        if ($user) {
            $usersOperador = User::where("type", "LIKE", "Operador")
                ->where("state", "=", 1)
                ->get();
            $usersOperadorD = User::where("type", "LIKE", "Operador")
                ->where("state", "=", 0)
                ->get();
            $usersSupervisor = User::where("type", "LIKE", "Supervisor")
                ->where("state", "=", 1)
                ->get();
            $usersSupervisorD = User::where("type", "LIKE", "Supervisor")
                ->where("state", "=", 0)
                ->get();

            return view("admin.users.listUsers", [
                "user" => $user,
                "usersOperadores" => $usersOperador,
                "usersOperadoresD" => $usersOperadorD,
                "usersSupervisores" => $usersSupervisor,
                "usersSupervisoresD" => $usersSupervisorD
            ]);
        }
        return redirect("/")->with("error", "No tiene permisos para ingresar a esta seccion, debe iniciar sesión.");
    }

    /**
     * @function - muestra el formulario para registrar un nuevo usuario
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    function formNewUser() {
        $user = Auth::user();
        if ($user) {
            return view("admin.users.formAddUser", [
                "user" => $user
            ]);
        }
        return redirect("/")->with("error", "No tiene permisos para ingresar a esta seccion, debe iniciar sesión.");
    }

    /**
     * @function - guarda un usuario sea un supervisor u operario
     * @return por json la clave para que el usuario nuevo pueda iniciar sesión
     */
    public function store() {

        $data = request()->all();
        $nuevo = User::create($data);
        $clave = time();
        $nuevo->password = Hash::make($clave);
        $nuevo->save();

        $obj = new ApiController();
        $ruta = $obj->subir_fotos(request()->profile_image, $nuevo->id);
        $nuevo->profile_image = $ruta;
        $nuevo->save();

        return response()->json($clave);
    }

    /**
     * Al asignarle meta a un usuario de tipo operador, también afecta la meta del usuario supervisor
     * @return type json
     */
    function asignarMetaHoy() {

        $user_id = decrypt(Input::get("user_id"));
        $meta = Input::get("meta");
        $fHoy = Carbon::now('America/Bogota')->format('Y-m-d');

        $userOp = User::findOrFail($user_id);
        //verifico si tiene el operador supervisor asignado
        if ($userOp->supervisor_id > 0) {
            $metaOp = $this->addMetaOp($userOp, $meta, $fHoy);      // asigna la meta de hoy al usuario operador
            $userSup = User::findOrFail($userOp->supervisor_id);    // busca el supervidor del uduario operador
            $metaSup = $this->addMetaSup($userSup, $fHoy);          // incrementa la meta del supervisor
            if ($metaOp && $metaSup) {
                return json_encode(array("status" => "ok", "msj" => "Se ha asignado la meta exitosamente."));
            }
        } else {
            return json_encode(array("status" => "fail", "msj" => "No se ha agregado la meta, el operador no tiene supervisor asignado."));
        }
        return json_encode(array("status" => "fail", "msj" => "Se ha almacenado la información de la meta asignada."));
    }

    /**
     * @function - muestra el formulario para editar los campos del usuario
     * @param - id del usuario a editar
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function edit(Request $request) {

        $user = Auth::user();
        if ($user) {
            $u = User::find(decrypt($request->id));
            $s = User::where('type', "Supervisor")->orderBy('user')->where('supervisor_id', "!=", $u->id)->pluck('nombres', 'id');
            return view("admin.users.formEditUser", [
                "user" => $user,
                "uEditar" => $u,
                "supervisores" => $s
            ]);
        }
        return redirect("/")->with("error", "No tiene permisos para ingresar a esta seccion, debe iniciar sesión.");
    }

    public function update(Request $request)
    {

        $u = User::find(decrypt($request->id));
        $u->nombres = $request->nombres;
        $u->apellidos = $request->apellidos;
        $u->email = $request->email;

        if ($request->profile_image) {
            $obj = new ApiController();
            $ruta = $obj->subir_fotos($request->profile_image, $u->id);
            $u->profile_image = $ruta;
        }

        if($request->reinicio == 1){
            $u->uuid = NULL;
        }

        $u->save();

        return response()->json(1);
    }

    public function deshabilitar() {

        $id = Input::get("id");
        if ($id) {
            $user = User::findOrFail(decrypt($id));
            $user->state = 0;
            if ($user->save()) {
                return json_encode(array("status" => "ok", "msj" => "Se ha deshabilitado el usuario exitosamente."));
            } else {
                return json_encode(array("status" => "fail", "msj" => "No se actualizado la información del usuario, por favor intentelo de nuevo."));
            }
        }
        return json_encode(array("status" => "fail", "msj" => "No se reconoce el identificador del usuario, por favor intentelo de nuevo."));
    }

    /**
     * @function - actualiza la meta diaria y global del operador
     * @param $userOp - usuario operador
     * @param $meta - meta a asignar al dia
     * @param $fHoy - fecha de hoy
     * @return bool - true si lo hace || false si no lo hace
     */
    protected function addMetaOp($userOp, $meta, $fHoy) {
        $metaAntOp = $userOp->meta_hoy;
        //resto a la meta_global la meta_hoy anterior para sumar la nueva
        if ($userOp->meta_global > 0) {
            $userOp->meta_global = $userOp->meta_global - $metaAntOp;
            $userOp->save();
        }
        //Asigno nueva meta_hoy
        $userOp->meta_hoy = $meta;
        //Agrego nueva meta_hoy a meta_global
        $userOp->meta_global = $userOp->meta_global + $meta;

        if ($userOp->save()) {
            //busco un registro de meta de hoy ya asignado
            $meta_hoy = DB::table('meta_operador')
                ->where("users_id", "=", $userOp->id)
                ->whereRaw("meta_operador.fecha = ?", array($fHoy))
                ->first();
            if ($meta_hoy) {
                //actualizo la meta de hoy
                DB::table('meta_operador')
                    ->where('meta_operador.users_id', "=", $userOp->id)
                    ->whereRaw("meta_operador.fecha = ?", array($fHoy))
                    ->update(['meta_operador.meta' => $meta]);
            } else {
                //creo nuevo registro de meta_hoy
                $meta_hooy = new MetaOperador();
                $meta_hooy->meta = $meta;
                $meta_hooy->fecha = $fHoy;
                $meta_hooy->users_id = $userOp->id;
                $meta_hooy->save();
            }

            return true;
        }
        return false;
    }

    protected function addMetaSup($userSup, $fHoy) {

        $sumMetaDia = DB::table("users")
            ->where("users.supervisor_id", "=", $userSup->id)
            ->sum("users.meta_hoy");

        $meta_hoy = DB::table('meta_supervisor')
            ->where("meta_supervisor.users_id", "=", $userSup->id)
            ->whereRaw("meta_supervisor.fecha = ?", array($fHoy))
            ->first();
        if ($meta_hoy) {
            //actualizo la meta de hoy
            DB::table('meta_supervisor')
                ->where('meta_supervisor.users_id', "=", $userSup->id)
                ->whereRaw("meta_supervisor.fecha = ?", array($fHoy))
                ->update(['meta_supervisor.meta' => $sumMetaDia]);
        } else {
            //creo nuevo registro de meta_hoy
            $meta_hooy = new MetaSupervisor();
            $meta_hooy->meta = $sumMetaDia;
            $meta_hooy->fecha = $fHoy;
            $meta_hooy->users_id = $userSup->id;
            $meta_hooy->save();
        }

        $sumMetaGlobal = DB::table('meta_supervisor')
            ->where('meta_supervisor.users_id', "=", $userSup->id)
            ->sum('meta_supervisor.meta');

        $userSup->meta_global = $sumMetaGlobal;
        $userSup->meta_hoy = $sumMetaDia;
        $userSup->save();

        return true;
    }

    /**
     * @function - habilita de nuevo a un usuario que nolo estaba dentro del sistema
     * @return json
     */
    function habilitarUser() {
        $id = Input::get("id");
        if ($id) {
            $user = User::findOrFail(decrypt($id));
            $user->state = 1;
            if ($user->save()) {
                return json_encode(array("status" => "ok", "msj" => "Se ha habilitado el usuario exitosamente."));
            } else {
                return json_encode(array("status" => "fail", "msj" => "No se actualizado la información del usuario, por favor intentelo de nuevo."));
            }
        }
        return json_encode(array("status" => "fail", "msj" => "No se reconoce el identificador del usuario, por favor intentelo de nuevo."));
    }

    /**
     * @funtion - dirige a la vista para asignar operadores a un supervisor indicado
     * @param $id - identificador interno encriptado del usuario supervisor para asignar operadores
     * @return View
     */
    function assingOperators($id) {
        $user = Auth::user();
        if ($user) {
            $userSup = User::findOrFail(decrypt($id));
            $opAsiggn = User::where("supervisor_id", "=", $userSup->id)
                ->where("state", "=", 1)
                ->get();
            $opNoAssing = User::where("supervisor_id", "=", NULL)
                ->where("type", "LIKE", "Operador")
                ->where("state", "=", 1)
                ->get();

            $fHoy = date("Y/m/d");
            $this->addMetaSup($userSup, $fHoy);

            return view("admin.users.assingUserOper", [
                'user' => $user,
                'opAssing' => $opAsiggn,
                'opNoAssing' => $opNoAssing,
                'id' => $id
            ]);
        }
        return redirect('/')->with("error", "No tiene permisos para realizar la acción indicada.");
    }

    /**
     * @function - asigna un operador a un supervisor
     * @return Json
     */
    function assignOpSup() {
        $userOp = User::findOrFail(decrypt(Input::get('idOp')));
        $userSup = User::findOrFail(decrypt(Input::get('idSup')));
        if ($userSup && $userOp) {
            $userOp->supervisor_id = $userSup->id;
            if ($userOp->save()) {
                return json_encode(array("status" => "ok", "msj" => "Se ha asignado el operador al supervisor exitosamente."));
            } else {
                return json_encode(array("status" => "fail", "msj" => "No se ha logrado asignar el operador al supervisor indicado, por favor intentelo de nuevo."));
            }
        }
        return json_encode(array("status" => "fail", "msj" => "No se reconoce la información de los usuarios indicada, por favor verifique e intentelo de nuevo."));
    }

    /**
     * @function - desasigna un operador a un supervisor
     * @return Json
     */
    function desAssignOpSup() {
        $userOp = User::findOrFail(decrypt(Input::get('idOp')));
        $userSup = User::findOrFail(decrypt(Input::get('idSup')));
        if ($userSup && $userOp) {
            $userOp->supervisor_id = 0;
            if ($userOp->save()) {
                return json_encode(array("status" => "ok", "msj" => "Se ha desasignado el operador al supervisor exitosamente."));
            } else {
                return json_encode(array("status" => "fail", "msj" => "No se ha logrado desasignar el operador al supervisor indicado, por favor intentelo de nuevo."));
            }
        }
        return json_encode(array("status" => "fail", "msj" => "No se reconoce la información de los usuarios indicada, por favor verifique e intentelo de nuevo."));
    }

    public function loginMovil(Request $request) {
        $credentials = $request->only('user', 'password');
        $token = null;
        try {
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json('invalid_user_or_password', 422);
            }
        } catch (JWTAuthException $e) {
            return response()->json('failed_to_create_token', 500);
        }

        $user = Input::get("user");
        $pass = Input::get("password");
        $uuid = Input::get("uuid");

        if ($user && $pass) {
            if (User::all()->first()->puede_registrar) {
                $usuario = User::where("user", "LIKE", $user)->where('type', "!=", 'Administrador')->first();
                if ($usuario) {
                    if (is_null($usuario->uuid)) {
                        $usuario->uuid = $uuid;
                        $usuario->save();
                        return response()->json(compact('token', 'usuario'));
                    }
                    if ($usuario->uuid == $uuid) {
                        return response()->json(compact('token', 'usuario'));
                    }
                    return response()->json('error_uuid', 422);
                }
                return response()->json('error_tipo_usuario', 422);
            }
            return response()->json('sistema_cerrado', 422);
        }
        return response()->json('sistema_cerrado', 422);
    }

    public function getAuthUser(Request $request) {
        $user = JWTAuth::toUser($request->token);
        return response()->json(['result' => $user]);
    }

    public function removeToken() {
        JWTAuth::invalidate(JWTAuth::getToken());
        return response()->json(['result' => 'remove token'], 200);
    }

    //------------
    public function validacionesSupervisor()
    {
        $idSup = Input::get("id");
        //Luminarias
        $sql = "SELECT activo.id as idActivo, activo.longitud, activo.latitud, activo.direccion, activo.nomenclatura, iluminacion.id as ilID, iluminacion.tipo as ilTipo, iluminacion.tecnologia as ilTecnologia, iluminacion.v_tecnologia, iluminacion.nombre as ilNombre, iluminacion.potencia as ilPotencia, iluminacion.v_potencia as ilVPotencia, iluminacion.foto as ilFoto, iluminacion.observacion as ilObs, iluminacion.novedades as ilNov, iluminacion.serial as ilSerial, apoyo.id as apId, apoyo.pertenece as apPertenece, apoyo.v_pertenece as apVPertenece, apoyo.tipo as apTipo, apoyo.v_tipo as apVTipo, apoyo.longitud as apLongitud, apoyo.v_longitud as apVLongitud, apoyo.observacion as apObs, apoyo.novedades as apNovedades, brazo.id as idBrz, brazo.tipo as brzTipo, brazo.v_tipo as brzVTipo, brazo.incorporado as brzIncorp, brazo.observacion as brzObs, brazo.novedades as brzNovedades, red.id as redId, red.tipo as redTipo, red.v_tipo as redVTipo, red.distancia as redDistancia, red.v_distancia as redVDistancia, red.aerea as redArerea, red.observacion as redObs, red.novedades as redNovedades, CONCAT(users.nombres, ' ', users.apellidos) as nombres FROM activo JOIN iluminacion ON iluminacion.activo_id = activo.id JOIN brazo ON brazo.iluminacion_id = activo.id JOIN apoyo ON apoyo.activo_id = activo.id JOIN red ON red.activo_id = activo.id JOIN users ON activo.users_id = users.id WHERE users.supervisor_id = ? AND (iluminacion.tipo = 1 OR iluminacion.tipo = 3) AND ((iluminacion.v_tecnologia = 1 OR iluminacion.v_potencia = 1) OR (brazo.v_tipo = 1) OR (apoyo.v_pertenece = 1 OR apoyo.v_tipo = 1 OR apoyo.v_longitud) OR (red.v_tipo = 1 OR red.v_distancia = 1)) LIMIT 10";
        $lumin = DB::select($sql, [$idSup]);
        //FarolesTerras
        $sqlFT = "SELECT activo.id as idActivo, activo.longitud, activo.latitud, activo.direccion, activo.nomenclatura, iluminacion.id as ilID, iluminacion.tipo as ilTipo, iluminacion.tecnologia as ilTecnologia, iluminacion.v_tecnologia as v_tecnologia, iluminacion.nombre as ilNombre, iluminacion.potencia as ilPotencia, iluminacion.v_potencia as ilVPotencia, iluminacion.observacion as ilObs, iluminacion.novedades as ilNov, red.id as redId, red.tipo as redTipo, red.v_tipo as redVTipo, red.distancia as redDistancia, red.v_distancia as redVDistancia, red.aerea as redArerea, red.observacion as redObs, red.novedades as redNovedades, CONCAT(users.nombres, ' ', users.apellidos) as nombres, users.profile_image FROM activo JOIN iluminacion ON iluminacion.activo_id = activo.id JOIN red ON red.activo_id = activo.id JOIN users ON users.id = activo.users_id WHERE users.supervisor_id = ? AND (iluminacion.tipo = 2 OR iluminacion.tipo = 4) AND (iluminacion.v_tecnologia = 1 OR iluminacion.v_potencia = 1 OR red.v_tipo = 1 OR red.v_distancia = 1) LIMIT 10";
        $ft = DB::select($sqlFT, [$idSup]);
        //Trafos
        $sqlTrf = "SELECT trafo.id as trafoID, trafo.potencia as trafoPotencia, trafo.latitud, trafo.longitud, trafo.observacion as trafoObs, trafo.serial as trafoSerial, trafo.foto as trafoFoto, trafo.trafo_validar, apoyo.pertenece as apPertenece, apoyo.v_pertenece as apVPertenece, apoyo.tipo as apTipo, apoyo.v_tipo as apVTipo, apoyo.longitud as apLong, apoyo.v_longitud as apVLong, apoyo.observacion as apObs, apoyo.novedades as apNovedades, CONCAT(users.nombres, ' ', users.apellidos) as nombres, users.profile_image FROM trafo  JOIN apoyo ON trafo.id = apoyo.trafo_id JOIN users ON users.id = trafo.users_id WHERE users.supervisor_id = ? AND (trafo.trafo_validar = 1 OR apoyo.v_pertenece = 1 OR apoyo.v_tipo = 1 OR apoyo.v_longitud = 1) LIMIT 10";
        $trf = DB::select($sqlTrf, [$idSup]);

        return response()->json(array('luminarias' => $lumin, 'faroles' => $ft,'trafos' => $trf));
    }


}
