<?php

namespace App\Http\Controllers;

// para manejar fechas ... 
use Carbon\Carbon;
use Excel;
use App\Activo;
use App\Trafo;
use App\User;
use App\Iluminacion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class SistemaController extends Controller {

    /**
     *
     * @return type
     */
    public function reportes(Request $request) {

        $user = Auth::user();
        $operadores = User::where('type', 'LIKE', 'Operador')->get();
        $supervisores = User::where('type', 'LIKE', 'Supervisor')->get();
        if ($request->input("operador")) {
            $operador = User::find($request->input("operador"));
            $f = $this->input2Carbon($request->input('fechas'));
            $desde = $f[0];
            $hasta = $f[1];
            $activos = $operador->activos()->whereBetween('created_at', [$desde, $hasta])->orderBy('created_at', 'ASC')->get();

            return view('admin.sistema.reportes')->with(["activos" => $activos, "user" => $user, "operador" => $operador, 'operadores' => $operadores, 'supervisores' => $supervisores]);
        }

        $activos = [];
        return view('admin.sistema.reportes')->with(["user" => $user, "activos" => $activos, 'operadores' => $operadores, 'supervisores' => $supervisores]);
    }

    /**
     * Metodo que genera una hoja de calculo con los datos de los activos y trafos
     * @param $request - El rango de fechas
     */
    public function generarHC(Request $request) {

        $f = $this->input2Carbon($request->input('fechas'));
        $desde = $f[0];
        $hasta = $f[1];

        Excel::create('Datos', function($excel) use($desde, $hasta) {

            $activos = Activo::whereBetween('created_at', [$desde, $hasta])->orderBy('created_at', 'ASC')->get();
            $trafos = Trafo::whereBetween('created_at', [$desde, $hasta])->orderBy('created_at', 'ASC')->get();

            // luminarias
            $excel->sheet('Luminarias', function($sheet) use($activos) {
                $sheet->loadView('admin.sistema.activosHC', ["activos" => $activos]);
            });

            // trafos
            $excel->sheet('trafos', function($sheet) use($trafos) {
                $sheet->loadView('admin.sistema.activosHC', ["activos" => $trafos]);
            });
        })->download('xls');
    }

    public function reporteMetas(Request $request) {

        $f = $this->input2Carbon($request->input('fechas'));
        $desde = $f[0];
        $hasta = $f[1];

        Excel::create('Datos', function($excel) use($desde, $hasta) {

            $operarios = User::whereBetween('created_at', [$desde, $hasta])
                ->where('type', "Operador")->orderBy('created_at', 'ASC')->get();

            // operadores
            $excel->sheet('Operadores', function($sheet) use($operarios) {
                $sheet->loadView('admin.sistema.metasOperadorHC', ["operarios" => $operarios]);
            });

            $supervisores = User::whereBetween('created_at', [$desde, $hasta])
                ->where('type', "Supervisor")->orderBy('created_at', 'ASC')->get();

            // supervisores
            $excel->sheet('Supervisores', function($sheet) use($supervisores) {
                $sheet->loadView('admin.sistema.metasSupervisorHC', ["supervisores" => $supervisores]);
            });
        })->download('xls');
    }

    private function input2Carbon($rango) {

        $fechas = array();
        //string(23) "08/04/2015 - 08/29/2015"
        $auxF = explode(" - ", $rango);
        $d = explode("/", $auxF[0]);
        $h = explode("/", $auxF[1]);

        $fechas[] = $desde = Carbon::create($d[2], $d[0], $d[1], 0, 0, 0);
        $fechas[] = $hasta = Carbon::create($h[2], $h[0], $h[1], 23, 59, 59);

        return $fechas;
    }

    public function generarKML() {

        $fHoy = Carbon::now('America/Bogota');
        $activos = Activo::get();
        $trafos = Trafo::get();

        $todos = $activos->merge($trafos);

        // crea el documento
        $dom = new \DOMDocument('1.0', 'UTF-8');

        // crea el elemento raíz y lo adiciona a la raíz del documento
        $node = $dom->createElementNS('http://earth.google.com/kml/2.1', 'kml');
        $parNode = $dom->appendChild($node);

        // crea un elemento kml y lo adiciona
        $dnode = $dom->createElement('Document');
        $docNode = $parNode->appendChild($dnode);

        foreach ($todos as $a) {

            // crea un placemark y lo adiciona al documento
            $node = $dom->createElement('Placemark');
            $placeNode = $docNode->appendChild($node);

            // crea un atributo id y le asigna el id del activo
            $placeNode->setAttribute('id', $a->id);

            // crea elementos nombre, y descripcion
            if ($a["table"] == "activo") {
                $nameNode = $dom->createElement('name', htmlentities("activo" .$a->id));
                $placeNode->appendChild($nameNode);
                $descNode = $dom->createElement('description', $a->fecha_registro. " " .$a->hora);
                $placeNode->appendChild($descNode);
            }else{
                $nameNode = $dom->createElement('name', htmlentities("trafo" .$a->id));
                $placeNode->appendChild($nameNode);
                $descNode = $dom->createElement('description', $a->observacion);
                $placeNode->appendChild($descNode);
            }

            // crea un elemento punto
            $pointNode = $dom->createElement('Point');
            $placeNode->appendChild($pointNode);

            // crea un elemento coordenada y le da el valor de longitud y latitud
            $coorStr = $a->longitud . ',' . $a->latitud;
            $coorNode = $dom->createElement('coordinates', $coorStr);
            $pointNode->appendChild($coorNode);
        }

        $kmlOutput = $dom->saveXML();
        Storage::disk('public')->put($fHoy . '.kml', $kmlOutput);

        return response()->download(storage_path("app/public/fotos/" . $fHoy . '.kml'));
    }

    public function reportesRevisor()
    {
        $user = Auth::user();
        $operadores = User::where('type', 'LIKE', 'Operador')->get();
        $supervisores = User::where('type', 'LIKE', 'Supervisor')->get();
        return view('revisor.reportes')->with(["user" => $user, 'operadores' => $operadores, 'supervisores' => $supervisores]);
    }

}
