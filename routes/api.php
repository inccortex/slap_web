<?php

use Illuminate\Http\Request;

/*
  |--------------------------------------------------------------------------
  | API Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register API routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | is assigned the "api" middleware group. Enjoy building your API!
  |
 */

Route::post("loginmovil", "UserController@loginMovil");

Route::post("validacionesSupervisor", "UserController@validacionesSupervisor");

Route::post("logioutnmovil", "UserController@removeToken");

Route::post('isOpen', function() {
    if (\App\User::all()->first()->puede_registrar) {
        return json_encode(["status" => "ok", "msj" => "Sistema abierto"]);
    }
    return json_encode(["status" => "fail", "msj" => "Sistema cerrado"]);
});

Route::post("saveValidacionSuperEI", "ApiController@saveValidacionSuperEI");
Route::post("saveValidacionSuperFT", "ApiController@saveValidacionSuperFT");
Route::post("saveValidacionSuperTRF", "ApiController@saveValidacionSuperTRF");


//     estas rutas ya están seguras
Route::group(['middleware' => 'jwt.auth'], function () {

    Route::get('user', 'UserController@getAuthUser');

    Route::get('logout', 'UserController@removeToken');


    Route::post('guardarLuminariaReflector', 'ApiController@guardarLuminariaReflector');
    Route::post('guardarFarol', 'ApiController@guardarFarol');
    Route::post('guardarTerreo', 'ApiController@guardarTerreo');
    Route::post('guardarTrafo', 'ApiController@guardarTrafo');

    Route::post('indicadoresOperador', ["as" => "indicadoresOperador", "uses" => "ApiController@indicadoresOperador"]);
    Route::post('indicadoresSupervisor', 'ApiController@indicadoresSupervisor');
    Route::post('getPendientes', 'ApiController@getPendientes');
});

