<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | En este archivo se encuentran las rutas que se utilizan para acceder a las
  | diferentes funcionalidades de la administraci贸n web
  |
 */

//Ruta para verificar la pagina que se muestra en el inicio
Route::get('/', "ApiController@getIndex");

Route::get('nuevasDirecciones', 'ActivoController@nuevasDirecciones');
Route::get('getActivos', 'ActivoController@getActivos');
Route::post('updateAddress', 'ActivoController@updateAddress');

Route::post("deleteActivos", "ActivoController@borrarActivos");

Route::post('uploadActivos', "ActivoController@editActivos");

/**
 * RUTAS LOGIN
 */

Route::post("login", "UserController@loginWeb");

Route::get("logout", "UserController@logoutWeb");

/**
 * RUTAS USERS
 */

Route::get("users", "UserController@users");

Route::get("users/formNew", "UserController@formNewUser");

Route::post("users/store", "UserController@store");

Route::get("users/editar/{id}", "UserController@edit");

Route::post("users/update", "UserController@update");

Route::post("users/metaHoy", "UserController@asignarMetaHoy");

Route::get("users/editar", "UserController@editar");

Route::post("users/hablitar", "UserController@habilitarUser");

Route::post("users/deshablitar", "UserController@deshabilitar");

Route::get("users/assingOperatos/{id}", "UserController@assingOperators");

Route::post("users/assignOpSup", "UserController@assignOpSup");

Route::post("users/desAssignOpSup", "UserController@desAssignOpSup");

Route::get("cerrarOperaciones", "ApiController@cerrarOperaciones");

Route::get("abrirOperaciones", "ApiController@abrirOperaciones");


/**
 * RUTAS DE INFORMES DEL SISTEMA
 */


Route::post("generarHC", "SistemaController@generarHC");

Route::any("reportes", "SistemaController@reportes");

Route::post("reporteActivos", "SistemaController@reporteActivos");

Route::get("generarKML", "SistemaController@generarKML");

Route::post("jsonRegistros", "ActivoController@jsonRegistros");

Route::get("datosGenerales", "ActivoController@datosGenerales");

Route::get("datosGeneralesOp", "ApiController@datosGeneralesOp");

Route::post("excelRegistros", "ActivoController@excelRegistros");

Route::get("revisor/reportes", "SistemaController@reportesRevisor");


